﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CanvasBattle : MonoBehaviour {

    /*
    TODO:
    Entry battle animation
    Enemies sorted in lanes
    Player sorted in lane huds
    List of next actors
    List of actions
    */

    #region Variables
    #region Editor
    [SerializeField]
    private GameObject actorBattlerPrefab;
    [SerializeField]
    private GameObject enemyBattlerPrefab;
    #endregion
    #region Public
    public bool isBusy
    {
        get
        {
            return hasPopups || hasAnimations;
        }
    }

    public bool hasPopups { get { return (m_popups.Count > 0); } }
    public bool hasAnimations { get { return (m_animations.Count > 0); } }
    #endregion
    #region Private
    private List<VisualActorBattler> m_actorVisuals;
    private List<VisualEnemyBattler> m_enemyVisuals;

    private Canvas m_canvas;
    #endregion
    #endregion

    public GameObject skillButtonPrefab;
    public GameObject popupTextPrefab;

    

    public GameObject skillListContent;

    public GameObject animationPrefab;

    private List<PopupText> m_popups;
    private List<GameObject> m_animations;

    private void Start()
    {
        m_canvas = GetComponent<Canvas>();
    }

    public void PlayAnimation(GameBattler a_target)
    {
        GameObject animObj = Instantiate(animationPrefab, GetVisualBattler(a_target).transform.position, Quaternion.identity);
        m_animations.Add(animObj);
    }

    public void Setup()
    {
        SetupBattlers();

        m_popups = new List<PopupText>();
        m_animations = new List<GameObject>();

        //SetSkillWindowActor(actors[0].battler);
    }

    [SerializeField]
    private RectTransform m_actorVisualsParent;
    [SerializeField]
    private RectTransform m_enemyVisualsParent;
    private void SetupBattlers()
    {
        // Actors
        m_actorVisuals = new List<VisualActorBattler>();
        List<GameBattler> actors = GameParty.instance.battleMembers;
        foreach(GameActor actor in actors)
        {
            VisualActorBattler actorVisual = Instantiate(actorBattlerPrefab, m_actorVisualsParent)
                .GetComponent<VisualActorBattler>();
            actorVisual.actor = actor;
            m_actorVisuals.Add(actorVisual);
        }
        // Enemies
        m_enemyVisuals = new List<VisualEnemyBattler>();
        List<GameBattler> enemies = GameTroop.instance.members;
        foreach(GameEnemy enemy in enemies)
        {
            VisualEnemyBattler enemyVisual = Instantiate(enemyBattlerPrefab, m_enemyVisualsParent)
                .GetComponent<VisualEnemyBattler>();
            enemyVisual.enemy = enemy;
            m_enemyVisuals.Add(enemyVisual);
        }
    }

    private void Update()
    {
        if (m_popups.Count > 0)
        {
            if (m_popups[0] == null) { m_popups.RemoveAt(0); }
            else { m_popups[0].gameObject.SetActive(true); }
        }
        if (m_animations.Count > 0)
        {
            m_animations.RemoveAll(anim => anim == null);
        }
    }

    public void RefreshStatus()
    {
        /*
        RefreshSkillWindow();
        foreach (BattlerComponent comp in actors) { comp.RefreshStates(); }
        foreach (BattlerComponent comp in enemies) { comp.RefreshStates(); }
        */
    }

    // start party command selection

    // command fight

    // command escape

    public void OpenPartyStatus()
    {

    }

    public void StartPartyCommandSelection()
    {
        // refresh status
        // status window open
        // actor window close
        // party window open
        //BattleManager.Instance.NextCommand();
    }

    public void StartActorCommandSelection()
    {
        // status window select actor
        // close party command window
        // open actor command window
        
        SetSkillWindowActor(BattleManager.Instance.actor);
    }

    // command attack
    // command skill
    // command guard?
    // command item

    // selection mode?

    // on actor ok/enemy okay?

    public void OnTurnStart()
    {
        // deselect party command window
        // close party command window
        // close actor command window
        // status window unselect
    }

    public void OnTurnEnd()
    {
        // close party command window
        // close actor command window
        // status window unselect
    }

    public void OnActionStart()
    {
        // status window open
    }

    public void OnActionEnd()
    {
        RefreshStatus();
        // display auto affect status
        // wait and clear
        // display current state
        // wait and clear
    }

    // log window wait

    // animation wait

    public BattlerComponent GetVisualBattler(GameBattler a_battler)
    {
        /*
        if (a_battler.isActor) { return actors.Find(actorVisual => actorVisual.battler == a_battler); }
        else { return enemies.Find(enemyVisual => enemyVisual.battler == a_battler); }
        */
        return null;
    }

    GameBattler skillWindowBattler;

    public void SetSkillWindowActor(GameBattler a_battler)
    {
        skillWindowBattler = a_battler;
        RefreshSkillWindow();
    }

    public void RefreshSkillWindow()
    {
        foreach (Transform trans in skillListContent.GetComponentsInChildren<Transform>()) if (trans != skillListContent.transform) Destroy(trans.gameObject);
        int i = 0;
        foreach (DataSkill skill in (skillWindowBattler as GameActor).skills)
        {
            GameObject button = Instantiate(skillButtonPrefab, skillListContent.transform);
            //button.transform.localPosition = Vector3.zero;
            button.GetComponent<RectTransform>().anchoredPosition = new Vector2(i % 2 == 0 ? -240 : 240, -15 -i / 2 * 45);
            button.GetComponent<Button>().onClick.AddListener(() => OnSkillOk(skill.id)); // skill command with skill id
            button.GetComponent<Button>().interactable = skillWindowBattler.IsUsable(skill);
            button.GetComponentInChildren<Text>().text = skill.displayName;
            i++;
        }
        //(skillListContent.transform as RectTransform).sizeDelta = new Vector2(0, 15 + 45 * (skillWindowBattler as GameActor).skills.Count / 2);
    }

    public void OnSkillOk(int a_skillId)
    {
        GameActor user = BattleManager.Instance.actor;
        user.input.SetSkill(a_skillId); // should set action
        // set last skill
        if (DataManager.skills[a_skillId].needSelection)
        {
            if (DataManager.skills[a_skillId].isForOpponent) { SelectEnemySelection(); }
            else { SelectActorSelection(); }
        }
        else
        {
            BattleManager.Instance.FinishActorCommandSelection();
        }

    }

    public enum Selection { none, party, actor, friend, enemy };

    public Selection currentSelection { get { return m_currentSelection; } }

    private int m_targetIndex;
    private Selection m_currentSelection;

    public void SelectActorSelection()
    {
        // actor window refresh
        // actor window show
        // actor window activate
        m_currentSelection = Selection.friend;
    }

    public void OnActorOk()
    {
        GameActor user = BattleManager.Instance.actor;
        user.currentAction.targetIndices.Add(m_targetIndex); // set actors targets
        if (user.currentAction.unselectedTargetsCount == 0)
        {
            // actor window hide
            // skill window hide
            // item window hide
            BattleManager.Instance.FinishActorCommandSelection();
        }
    }

    public void SelectEnemySelection()
    {
        // enemy window refresh
        // enemy window show
        // enemy window activate
        m_currentSelection = Selection.enemy;
    }

    public void OnEnemyOk()
    {
        GameActor user = BattleManager.Instance.actor;
        user.currentAction.targetIndices.Add(m_targetIndex); // set actors targets
        if (user.currentAction.unselectedTargetsCount <= 0)
        {
            // enemy window hide
            // skill window hide
            // item window hide
            BattleManager.Instance.FinishActorCommandSelection();
        }
    }

    public void SetTarget(int a_targetIndex)
    {
        m_targetIndex = a_targetIndex;
    }

    public void DisplayResults(GameActionResult a_result)
    {
        if (a_result.missed) { DisplayBattlerPopup("Missed", a_result.target); }
        if (a_result.hpDamage > 0) { DisplayDamage(a_result.hpDamage, a_result.target); }
        foreach(int stateId in a_result.addedStates) { DisplayStateAdded(stateId, a_result.target); }
    }

    public void DisplayDamage(int damage, GameBattler a_target)
    {
        DisplayBattlerPopup(damage.ToString(), a_target);
    }

    public void DisplayStateAdded(int a_stateId, GameBattler a_target)
    {
        DataState state = DataManager.states[a_stateId];
        DisplayBattlerPopup(state.displayName, a_target);
    }

    public void DisplayBattlerPopup(string a_text, GameBattler a_target)
    {
        BattlerComponent comp = GetVisualBattler(a_target);
        Vector3 position = Camera.main.WorldToScreenPoint(comp.transform.position + Vector3.up * 1.2f);
        DisplayTextPopup(a_text, position);
    }

    public void DisplayTextPopup(string a_text, Vector3 a_position)
    {
        GameObject popup = Instantiate(popupTextPrefab, a_position, Quaternion.identity, transform);
        popup.GetComponent<Text>().text = a_text;
        popup.SetActive(false);
        m_popups.Add(popup.GetComponent<PopupText>());
    }

}
