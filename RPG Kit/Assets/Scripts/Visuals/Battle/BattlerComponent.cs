﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(SpriteRenderer))]
public class BattlerComponent : MonoBehaviour {

    public GameObject healthBarPrefab;
    public GameBattler battler { get; private set; }

    private SpriteRenderer m_spriteRenderer;

    private GameObject m_healthBar;

    private List<GameObject> m_states;

    private CanvasBattle m_canvas;

    // Use this for initialization
    void Awake () {
        // initialize graphics and everything
        m_spriteRenderer = GetComponent<SpriteRenderer>();
	}

    private void Start()
    {
        m_canvas = FindObjectOfType<CanvasBattle>();    
    }

    public void SetBattler(GameBattler a_battler)
    {
        battler = a_battler;
        m_spriteRenderer.sprite = a_battler.battlerSprite;
        if (a_battler.isEnemy) { m_spriteRenderer.flipX = true; }
        transform.position = (a_battler.isActor ? Vector3.left : Vector3.right) * 2 * (a_battler.position + 1);
        m_healthBar = Instantiate(healthBarPrefab, FindObjectOfType<CanvasBattle>().transform);
        m_healthBar.GetComponent<Healthbar>().battler = battler;
        Vector2 ScreenPoint = Camera.main.WorldToScreenPoint(transform.position + Vector3.down * 1.2f);
        m_healthBar.transform.position = ScreenPoint;
        RefreshStates();
    }

    public Font m_font;
    public void RefreshStates()
    {
        if (m_states != null) foreach (GameObject obj in m_states) { Destroy(obj); }
        m_states = new List<GameObject>();
        int i = 0;
        foreach (DataState state in battler.states)
        {
            GameObject obj = new GameObject();
            Image imgComp = obj.AddComponent<Image>();
            imgComp.sprite = state.icon;
            obj.transform.SetParent(m_healthBar.transform);
            obj.transform.position = m_healthBar.transform.position;
            obj.GetComponent<RectTransform>().sizeDelta = new Vector2(24, 24);
            obj.GetComponent<RectTransform>().anchoredPosition += new Vector2(-36 + i * 24, -24);
            if (battler.StateStacks(state.id) > 1)
            {
                GameObject stacksObj = new GameObject();
                Text txtComp = stacksObj.AddComponent<Text>();
                txtComp.text = battler.StateStacks(state.id).ToString();
                //txtComp.fontSize = ;
                txtComp.font = m_font;
                stacksObj.transform.SetParent(obj.transform);
                stacksObj.transform.localPosition = Vector3.zero;
                stacksObj.GetComponent<RectTransform>().anchoredPosition += new Vector2(0, -24);
                stacksObj.GetComponent<RectTransform>().pivot = new Vector3(0, 0.5f);
                stacksObj.GetComponent<RectTransform>().sizeDelta = new Vector3(100, 24);
                //obj.GetComponent<RectTransform>().sizeDelta = new Vector2(24, 24);
                //obj.GetComponent<RectTransform>().anchoredPosition += new Vector2(-36 + i * 24, -24);
            }
            m_states.Add(obj);
            i++;
        }
    }

    private void OnMouseDown()
    {
        if (m_canvas.currentSelection == CanvasBattle.Selection.enemy)
        {
            if (battler.isEnemy)
            {
                m_canvas.SetTarget(battler.position);
                m_canvas.OnEnemyOk();
            }
        }
        else if (m_canvas.currentSelection == CanvasBattle.Selection.friend)
        {
            if (battler.isActor)
            {
                m_canvas.SetTarget(battler.position);
                m_canvas.OnActorOk();
            }
        }
    }
}
