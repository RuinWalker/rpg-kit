﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualEnemyBattler : MonoBehaviour {

    #region Variables
    #region Editor
    [SerializeField]
    private Image m_battlerImage;

    [SerializeField]
    private RectTransform m_hpBar;

    [SerializeField]
    private Image m_hpBarFill;
    #endregion

    #region Public
    public GameEnemy enemy { get { return m_enemy; } set { m_enemy = value; Initialize(); } }
    #endregion

    #region Private
    private GameEnemy m_enemy;
    #endregion
    #endregion

    private void Initialize()
    {
        (transform as RectTransform).anchoredPosition = new Vector2(m_enemy.position * 100 - 200f, -46f);
        m_battlerImage.sprite = m_enemy.battlerSprite;
        m_battlerImage.SetNativeSize();
        m_hpBarFill.fillAmount = 1;
    }
}
