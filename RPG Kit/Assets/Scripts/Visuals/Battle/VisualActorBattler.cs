﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VisualActorBattler : MonoBehaviour
{
    [SerializeField]
    private Image m_battlerImage;

    [SerializeField]
    private RectTransform m_hpBar;

    [SerializeField]
    private Image m_hpBarFill;

    public GameActor actor { get { return m_actor; } set { m_actor = value; Initialize(); } }

    private GameActor m_actor;

    private void Initialize()
    {
        (transform as RectTransform).anchoredPosition = new Vector2(m_actor.position * 80 - 160, 0);
        m_battlerImage.sprite = m_actor.battlerSprite;
        m_battlerImage.SetNativeSize();
        m_hpBarFill.fillAmount = m_actor.hpPercentage;
    }
}
