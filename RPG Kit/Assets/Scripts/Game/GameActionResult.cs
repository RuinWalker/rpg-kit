﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameActionResult {

    public GameBattler user { get; private set; }
    public GameBattler target { get; private set; }
    public DataUsableItem item { get; private set; }
    public bool missed { get; set; }
    public bool critical { get; set; }
    private int m_hpDamage;
    private int m_mpDamage;
    private int m_hpDrain;
    private int m_mpDrain;
    public int hpRecovery { get; set; }
    public int mpRecovery { get; set; }
    private List<int> m_addedStates;
    private List<int> m_removedStates;

    public int hpDamage { get { return m_hpDamage; } }
    public int mpDamage { get { return m_hpDamage; } }
    public int hpDrain{ get { return m_hpDrain; } }
    public int mpDrain { get { return m_mpDrain; } }
    public List<int> addedStates { get { return m_addedStates; } }
    public List<int> removedStates { get { return m_removedStates; } }

    public bool isHit { get { return !missed; } }

    public GameActionResult(GameBattler a_target)
    {
        target = a_target;
        Clear();
    }

    public void Clear()
    {
        user = null;
        item = null;
        ClearHitFlags();
        ClearDamageValues();
        ClearStatusEffects();
    }

    public void ClearHitFlags()
    {
        missed = false;
        critical = false;
    }

    public void ClearDamageValues()
    {
        m_hpDamage = 0;
        m_mpDamage = 0;
        m_hpDrain = 0;
        m_mpDrain = 0;
    }

    public void ClearStatusEffects()
    {
        m_addedStates = new List<int>();
        m_removedStates = new List<int>();
    }

    public void SetItem(GameBattler a_user, DataUsableItem a_item)
    {
        user = a_user;
        item = a_item;
    }

    public void MakeDamageValue(int value, DataUsableItem a_item)
    {
        if (a_item.damage.isToHp) { m_hpDamage = value; }
        if (a_item.damage.isToMp) { Debug.Log("Has mp damage"); m_mpDamage = value; }
        m_mpDamage = System.Math.Min(target.currentMp, m_mpDamage);
        if (a_item.damage.isDrain)
        {
            m_hpDrain = (int)(m_hpDamage * a_item.damage.drainValue);
            m_mpDrain = (int)(m_mpDamage * a_item.damage.drainValue);
        }
    }
}
