﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTroop : GameUnit
{
    static public GameTroop instance
    {
        get
        {
            if (m_instance == null) { m_instance = new GameTroop(); }
            return m_instance;
        }
        set { m_instance = value; }
    }
    static private GameTroop m_instance;

    private int m_turnCount;

    public GameTroop() : base()
    {
        Clear();
    }

    public void Clear()
    {
        members.Clear();
        m_turnCount = 0;
    }

    public void Setup() //int a_troopId)
    {
        Clear();
        // set troop id
        // from data each member do
        AddEnemy();
        AddEnemy();
        // init screen tone
        // make unique names
    }

    public void AddEnemy()
    {
        // next unless enemy exists
        GameEnemy enemy = new GameEnemy(0); // make enemy
        enemy.position = firstOpenPosition; // position should be retrieved from troop
        members.Add(enemy);
        slots[enemy.position] = members[members.Count - 1];
        // hide
        // set screen pos
        // add to array
    }
}
