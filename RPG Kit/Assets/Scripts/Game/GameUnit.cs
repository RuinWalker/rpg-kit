﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameUnit  {

    public List<GameBattler> members { get { return m_members; } }
    public List<GameBattler> aliveMembers { get { return m_members.Where(member => member.isAlive).ToList<GameBattler>(); } }
    public List<GameBattler> deadMembers { get { return m_members.Where(member => member.isDead).ToList<GameBattler>(); } }
    public GameBattler[] slots { get { return m_slots; } }

    private List<GameBattler> m_members;
    private GameBattler[] m_slots; // first index is front row
    private bool m_inBattle;

    public bool isInBattle { get { return m_inBattle;  } }

	// Use this for initialization
	public GameUnit () {
        m_members = new List<GameBattler>();
        m_slots = new GameBattler[5] { null, null, null, null, null };
	}
	
    public void OnBattleStart()
    {
        foreach (GameBattler member in members) member.OnBattleStart();
        m_inBattle = true;
    }

    public void OnBattleEnd()
    {
        m_inBattle = false;
        foreach (GameBattler member in members) member.OnBattleEnd();
    }

    public void MakeActions()
    {
        foreach (GameBattler member in members) { member.MakeActions(); }
    }

    public bool areAllDead { get { return aliveMembers.Count == 0; } }

    public int firstOpenPosition
    {
        get
        {
            int result = -1;
            for (int i = 0; i < m_slots.Length; i++)
            {
                if (m_slots[i] == null)
                {
                    result = i;
                    break;
                }
            }
            return result;
        }
    }

    public void MoveForward(int a_currentIndex, int a_displacement)
    {
        MovePosition(a_currentIndex, -a_displacement);
    }

    public void MoveBackward(int a_currentIndex, int a_displacement)
    {
        MovePosition(a_currentIndex, a_displacement);
    }

    public void MovePosition(int a_currentIndex, int a_displacment)
    {
        int newIndex = Math.Max(Math.Min(a_currentIndex + a_displacment, m_slots.Length), 0);
        int min, max;
        if (a_displacment < 0)
        {
            min = newIndex;
            max = a_currentIndex;
        }
        else
        {
            min = a_currentIndex;
            max = newIndex;
        }
        for (int i = min; i < max; i++) { SwapPosition(i, i + 1); }
    }

    public void SwapPosition(int a_currentIndex, int a_newIndex)
    {
        GameBattler temp = m_slots[a_currentIndex];
        m_slots[a_currentIndex] = m_slots[a_newIndex];
        m_slots[a_newIndex] = temp;
        m_slots[a_currentIndex].position = a_currentIndex;
        m_slots[a_newIndex].position = a_newIndex;
    }

    public void ShufflePosition(int a_currentIndex, int a_displacment)
    {
        // Knuth shuffle algorithm
        List<GameBattler> original = new List<GameBattler>(m_slots);
        for (int i = 0; i < m_slots.Length; i++)
        {
            int indexToGrab = UnityEngine.Random.Range(i, original.Count);
            m_slots[i] = original[indexToGrab];
            original.RemoveAt(indexToGrab);
        }
        for (int i = 0; i < m_slots.Length; i++) { m_slots[i].position = i; }
    }

}
