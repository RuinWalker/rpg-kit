﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;

public class GameActor : GameBattler {

    private int m_actorId;
    private int m_actionInputIndex;
    private int m_classId;
    private int m_level;
    private DataEquipItem[] m_equips;
    private List<int> m_skills;
    private Dictionary<int, int> m_exp;

    public override Sprite battlerSprite { get { return actor.battlerSprite; } }

    public GameActor(int a_actorId) : base()
    {
        Setup(a_actorId);
    }

    private void Setup(int a_actorId)
    {
        m_actorId = a_actorId;
        battlerName = actor.displayName;
        // graphics
        m_classId = actor.classId;
        m_level = actor.initialLevel;
        // exp
        InitEquips();
        InitExp();
        InitSkills();
        // init equips
        // clear param plus
        RecoverAll();
    }

    public DataActor actor { get { return DataManager.actors[m_actorId]; } }

    public int ExpForLevel(int a_level)
    {
        return (actor.useActorExp ? actor.ExpForLevel(a_level) : actorClass.ExpForLevel(a_level));
    }

    public void InitExp()
    {
        m_exp = new Dictionary<int, int>();
        m_exp.Add(m_classId, currentLevelExp);
    }

    public int exp
    {
        get { return m_exp[m_classId]; }
    }

    public int currentLevelExp { get { return ExpForLevel(m_level); } }

    public int nextLevelExp { get { return ExpForLevel(m_level + 1); } }

    public int maxLevel { get { return actor.maxLevel; } }

    public bool isMaxLevel { get { return m_level >= maxLevel; } }

    public void InitSkills()
    {
        m_skills = new List<int>();
        foreach (DataClass.Learning learning in actorClass.learnings)
        {
            if (learning.level <= m_level) { LearnSkill(learning.skill); }
        }
    }

    public void InitEquips()
    {
        m_equips = new DataEquipItem[DataEquipItem.slots.Length];
        if (actor.equips != null)
        {
            for (int i = 0; i < m_equips.Length && i < actor.equips.Length; i++)
            {
                m_equips[i] = actor.equips[i];
            }
        }
        Refresh();
    }

    public List<DataWeapon> weapons
    {
        get
        {
            List<DataWeapon> result = new List<DataWeapon>();
            foreach(DataEquipItem equip in equips) { if (equip.isWeapon) { result.Add(equip as DataWeapon); } }
            return result;
        }
    }

    public List<DataArmor> armors
    {
        get
        {
            List<DataArmor> result = new List<DataArmor>();
            foreach (DataEquipItem equip in equips) { if (equip.isArmor) { result.Add(equip as DataArmor); } }
            return result;
        }
    }

    public List<DataWeapon> equips
    {
        get
        {
            List<DataWeapon> result = new List<DataWeapon>();
            foreach (DataEquipItem equip in equips) { if (equip.isWeapon) { result.Add(equip as DataWeapon); } }
            return result;
        }
    }

    public bool IsEquipChangeOk(int a_slotId)
    {
        // return false if equip type fixed
        // return false if equip type sealed
        return true;
    }

    public void ChangeEquip(int a_slotId, DataEquipItem a_item)
    {
        if (!TradeItemWithParty(a_item, m_equips[a_slotId]))
        if (a_item && DataEquipItem.slots[a_slotId] != a_item.equipType) { return; }
        m_equips[a_slotId] = a_item;
        Refresh();
    }

    public bool TradeItemWithParty(DataEquipItem newItem, DataEquipItem oldItem)
    {
        // false if new item and not party has new item
        // party gain old item
        // party lose one of new item
        return true;
    }

    public bool IsEquippable(DataEquipItem a_item)
    {
        return true;
    }

    public void ReleaseUnequippableItems(bool a_itemGain)
    {
        while(true)
        {
            bool somethingChanged = false;
            for(int i = 0; i < m_equips.Length; i++)
            {
                if (m_equips[i] == null || (IsEquippable(m_equips[i]) && m_equips[i].equipType == DataEquipItem.slots[i]))
                    continue;
                somethingChanged = true;
                if (a_itemGain) { TradeItemWithParty(null, m_equips[i]); } // change to gain item
                m_equips[i] = null;
            }
            if (!somethingChanged)
                return;
        }
    }

    public void ClearEquipment()
    {
        for (int i = 0; i < m_equips.Length; i++)
        {
            if (IsEquipChangeOk(i)) { ChangeEquip(i, null); }
        }
    }

    public bool IsSkillWtypeOk(DataSkill a_skill)
    {
        if (a_skill.requiredWeaponTypes.Count == 0) { return true; }
        if (a_skill.requiredWeaponTypes.Contains(DataWeapon.WeaponType.none)) { return true; }
        foreach (DataWeapon weapon in weapons)
        {
            if (a_skill.requiredWeaponTypes.Contains(weapon.weaponType)) { return true; }
        }
        return false;
    }

    public override void Refresh()
    {
        ReleaseUnequippableItems(true);
        base.Refresh();
    }

    public override bool isActor { get { return true; } }

    public override GameUnit friendsUnit
    {
        get { return GameParty.instance; }
    }

    public override GameUnit opponentsUnit
    {
        get { return GameTroop.instance; }
    }

    public DataClass actorClass { get { return DataManager.classes[m_classId]; } }

    public bool isBattleMember { get { return GameParty.instance.battleMembers.Contains(this); } }

    public List<DataSkill> skills
    {
        get
        {
            List<DataSkill> result = new List<DataSkill>();
            if (m_skills != null)
            {
                foreach (int skillId in m_skills) { result.Add(DataManager.skills[skillId]); }
            }
            return result;
        }
    }

    public List<DataSkill> usableSkills { get { return skills.Where(skill => IsUsable(skill)).ToList(); } }

    public override List<DataBaseItem> featureObjects
    {
        get
        {
            List<DataBaseItem> result = base.featureObjects;
            result.Add(actor);
            result.Add(actorClass);
            foreach(DataBaseItem skill in skills) { result.Add(skill); }
            return result;
        }
    }

    // TODO: atk elements

    public override int ParamBase(int a_paramId)
    {
        if (actor.useActorStats) { return actor.parameters[m_level + a_paramId * (maxLevel + 1)]; }
        else { return actorClass.parameters[m_level + a_paramId * (actorClass.maxLevel + 1)]; }
    }

    public override int ParamPlus(int a_paramId)
    {
        int result = base.ParamPlus(a_paramId);
        foreach (DataEquipItem equip in equips) { result += equip.parameters[a_paramId]; }
        return result;
    }
    
    public void ChangeExp(int a_amount, bool a_show)
    {
        m_exp[m_classId] = Math.Max(a_amount, 0);
        //int lastLevel = m_level;

        while (!isMaxLevel && exp >= nextLevelExp) { LevelUp(); }
        while (exp < currentLevelExp) { LevelDown(); }
        // display level up
    }

    public void LevelUp()
    {
        m_level++;
        foreach (DataClass.Learning learning in actorClass.learnings)
        {
            if (learning.level == m_level) { LearnSkill(learning.skill); }
        }
    }

    public void LevelDown()
    {
        m_level--;
    }

    public void LearnSkill(DataSkill a_skill)
    {
        if (!HasLearnedSkill(a_skill))
        {
            m_skills.Add(DataManager.GetIndex(a_skill));
        }
    }

    public void LearnSkill(int a_skillId)
    {
        if (!HasLearnedSkill(DataManager.skills[a_skillId]))
        {
            m_skills.Add(a_skillId);
        }
    }

    public void ForgetSkill(int a_skillId) { m_skills.Remove(a_skillId); }

    public bool HasLearnedSkill(DataBaseItem skill) { return skill.isSkill && m_skills.Contains(skill.id); }

    public override void ClearActions()
    {
        base.ClearActions();
        m_actionInputIndex = 0;
    }

    public GameAction input { get { return actions[m_actionInputIndex]; } }

    public bool NextCommand()
    {
        return ToCommand(1);
    }

    public bool PriorCommand()
    {
        return ToCommand(-1);
    }

    public bool ToCommand(int a_direction)
    {
        a_direction = a_direction < 0 ? -1 : 1;
        if (m_actionInputIndex <= 0 || m_actionInputIndex >= actions.Count - 1) return false;
        m_actionInputIndex += a_direction;
        return true;
    }
}
