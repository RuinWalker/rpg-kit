﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameParty : GameUnit {

    static public GameParty instance
    {
        get
        {
            if (m_instance == null) { m_instance = new GameParty(); }
            return m_instance;
        }
        set { m_instance = value; }
    }
    static private GameParty m_instance;

    public List<GameBattler> battleMembers { get { return members.GetRange(0, Mathf.Min(members.Count, 3)); } } // TODO: the 3 needs to be set somewhere in settings

    public GameParty() : base()
    {
        AddActor(0);
    }

    public void AddActor(int a_actorId)
    {
        members.Add(new GameActor(a_actorId));
        int index = firstOpenPosition;
        if (index >= 0)
        {
            slots[index] = members[members.Count - 1];
            members[members.Count - 1].position = index;
        }
        // game player refresh
        // game map need refresh = true
    }

    public bool isInputable { get { return members.Any(battler => battler.isInputable); } }

}
