﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class GameBattler {

    #region Constants
    /// <summary>
    /// The ID of the death state. Needs to be moved to a Settings ScriptableObject.
    /// </summary>
    const int c_deathStateId = 0;
    #endregion

    #region Enums
    public enum Parameter { mhp, mmp, atk, def, mat, mdf, agi, luk }
    public enum ExtraParameter { hit, evasion, mtp, criticalRate, criticalEvasion, magicEvasion, magicReflection, hpRegenFlat, mpRegenFlat, tpRegenFlat, hpRegenRate, mpRegenRate,  tpRegenRate }
    public enum SpecialParameter { }

    public enum SpecialFlags { preserveTp }
    #endregion

    #region Variables
    public int currentHp
    {
        get { return m_currentHp; }
        set { m_currentHp = value; Refresh(); }
    }
    private int m_currentHp;

    public int currentMp
    {
        get { return m_currentMp; }
        set { m_currentMp = value; Refresh(); }
    }
    private int m_currentMp;

    public int currentTp
    {
        get { return m_currentTp; }
        set { m_currentTp = value; Refresh(); }
    }
    private int m_currentTp;

    public int mhp { get { return Param((int)Parameter.mhp); } }
    public int mmp { get { return Param((int)Parameter.mmp); } }
    public int mtp { get { return (int)XParam((int)ExtraParameter.mtp); } }
    public int atk { get { return Param((int)Parameter.atk); } }
    public int def { get { return Param((int)Parameter.def); } }
    public int mat { get { return Param((int)Parameter.mat); } }
    public int mdf { get { return Param((int)Parameter.mdf); } }
    public int agi { get { return Param((int)Parameter.agi); } }
    public int luk { get { return Param((int)Parameter.luk); } }
    public float hit;
    public float eva;
    public float mev;

    private int[] m_paramPlus;

    private List<int> m_states;
    private Dictionary<int, int> m_stateTurns;
    private Dictionary<int, int> m_stateStacks;

    public int index { get { return friendsUnit.members.IndexOf(this); } }

    public int position
    {
        get { return m_position; }
        set { m_position = value; }
    }
    private int m_position;

    public List<GameAction> actions { get { return m_actions; } }
    private List<GameAction> m_actions;
    public GameActionResult actionResult { get { return m_actionResult; } }
    private GameActionResult m_actionResult;

    public string battlerName { get { return m_battlerName; } protected set { m_battlerName = value; } }
    private string m_battlerName;

    public virtual Sprite battlerSprite { get { return null; } }
    #endregion

    #region Constructors
    public GameBattler()
    {
        m_currentHp = m_currentMp = m_currentTp = 0;
        m_paramPlus = new int[Enum.GetNames(typeof(Parameter)).Length];

        ClearStates();

        m_actions = new List<GameAction>();
        m_actionResult = new GameActionResult(this);
        Debug.Log(m_actionResult + " created");
        m_battlerName = "";
    }
    #endregion

    #region States
    public void EraseState(int a_stateId)
    {
        m_states.Remove(a_stateId);
        m_stateTurns.Remove(a_stateId);
        m_stateStacks.Remove(a_stateId);
    }

    public bool HasState(int a_stateId)
    {
        return m_states.FindIndex(stateId => stateId == a_stateId) > -1;
    }

    public bool HasDeathState()
    {
        return HasState(c_deathStateId);
    }

    /// <summary>
    /// Get all states this battler is affected with.
    /// </summary>
    public List<DataState> states
    {
        get
        {
            List<DataState> result = new List<DataState>();
            foreach (int stateId in m_states)
            {
                result.Add(DataManager.states[stateId]);
            }
            return result;
        }
    }

    /// <summary>
    /// Get the stacks for the supplied state.
    /// </summary>
    /// <param name="a_stateId"></param>
    /// <returns></returns>
    public int StateStacks(int a_stateId)
    {
        if (!HasState(a_stateId)) return 0;
        return m_stateStacks[a_stateId];
    }
    #endregion

    #region Features
    /// <summary>
    /// Get all objects whose features affect this battler.
    /// </summary>
    public virtual List<DataBaseItem> featureObjects
    {
        get
        {
            List<DataBaseItem> result = new List<DataBaseItem>();
            foreach(DataState state in states)
            {
                int stacks = state.featuresOnlyOnce ? 1 : StateStacks(state.id);
                for (int i = 0; i < stacks; i++) { result.Add(state); }
            }
            return result;
        }
    }

    /// <summary>
    /// Get all features which affect this battler.
    /// </summary>
    List<DataBaseItem.Feature> allFeatures
    {
        get
        {
            List<DataBaseItem.Feature> result = new List<DataBaseItem.Feature>();
            foreach (DataBaseItem item in featureObjects) { result.AddRange(item.features); }
            return result;
        }
    }

    /// <summary>
    /// Get all features with the supplied code.
    /// </summary>
    /// <param name="a_code"></param>
    /// <returns></returns>
    List<DataBaseItem.Feature> GetFeatures(DataBaseItem.Feature.Code a_code)
    {
        return allFeatures.Where(feature => feature.code == a_code).ToList();
    }

    /// <summary>
    /// Get all features with the supplied code and data id.
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    List<DataBaseItem.Feature> GetFeatures(DataBaseItem.Feature.Code a_code, int a_id)
    {
        return GetFeatures(a_code).Where(feature => feature.dataId == a_id).ToList();
    }

    /// <summary>
    /// Get the results of all features with the supplied code and id multiplied.
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    float GetFeaturesMultiplied(DataBaseItem.Feature.Code a_code, int a_id)
    {
        List<DataBaseItem.Feature> features = GetFeatures(a_code, a_id);
        float result = 1f;
        foreach (DataBaseItem.Feature feature in features)
        {
            result *= feature.value;
        }
        return result;
    }

    /// <summary>
    /// Get the results of all features with the supplied code and id summed.
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    float GetFeaturesSum(DataBaseItem.Feature.Code a_code, int a_id)
    {
        List<DataBaseItem.Feature> features = GetFeatures(a_code, a_id);
        float result = 0f;
        foreach (DataBaseItem.Feature feature in features)
        {
            result += feature.value;
        }
        return result;
    }

    /// <summary>
    /// Get the results of all features with the supplied code and id in a list.
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    List<int> GetFeaturesSet(DataBaseItem.Feature.Code a_code, bool a_unique = false)
    {
        List<DataBaseItem.Feature> features = GetFeatures(a_code);
        List<int> result = new List<int>();
        foreach (DataBaseItem.Feature feature in features)
        {
            if (a_unique && result.Contains(feature.dataId)) { continue; }
            result.Add(feature.dataId);
        }
        return result;
    }

    /// <summary>
    /// Get the results of all features with the supplied code and id in a list (unique results).
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    List<int> GetFeaturesSetUnique(DataBaseItem.Feature.Code a_code)
    {
        return GetFeaturesSet(a_code, true);
    }

    /// <summary>
    /// Get the asset results of all features with the supplied code and id in a list.
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    List<DataBaseItem> GetFeaturesData(DataBaseItem.Feature.Code a_code, bool a_unique = false)
    {
        List<DataBaseItem.Feature> features = GetFeatures(a_code);
        List<DataBaseItem> result = new List<DataBaseItem>();
        foreach (DataBaseItem.Feature feature in features)
        {
            if (a_unique && result.Contains(feature.valueData)) { continue; }
            result.Add(feature.valueData);
        }
        return result;
    }

    /// <summary>
    /// Get the asset results of all features with the supplied code and id in a list (unique results).
    /// </summary>
    /// <param name="a_code"></param>
    /// <param name="a_id"></param>
    /// <returns></returns>
    List<DataBaseItem> GetFeaturesDataUnique(DataBaseItem.Feature.Code a_code)
    {
        return GetFeaturesData(a_code, true);
    }
    #endregion

    #region Parameters
    /// <summary>
    /// Return the base value for the parameter.
    /// </summary>
    /// <param name="a_paramId"></param>
    /// <returns></returns>
    public virtual int ParamBase(int a_paramId)
    {
        return 0;
    }

    public virtual int ParamPlus(int a_paramId)
    {
        return m_paramPlus[a_paramId];
    }

    /// <summary>
    /// Get the minimum value for the parameter.
    /// </summary>
    /// <param name="a_paramId"></param>
    /// <returns></returns>
    public int GetParamMin(int a_paramId)
    {
        switch(a_paramId)
        {
            default: return 1;
        }
    }

    /// <summary>
    /// Get the maximum value for the parameter.
    /// </summary>
    /// <param name="a_paramId"></param>
    /// <returns></returns>
    public int GetParamMax(int a_paramId)
    {
        switch (a_paramId)
        {
            default: return 9999;
        }
    }

    /// <summary>
    /// Get the flat bonus to the parameter.
    /// </summary>
    /// <param name="a_paramId"></param>
    /// <returns></returns>
    public int ParamSum(int a_paramId)
    {
        return (int)GetFeaturesSum(DataBaseItem.Feature.Code.Param, a_paramId);
    }

    /// <summary>
    /// Get the bonus rate of the parameter.
    /// </summary>
    /// <param name="a_paramId"></param>
    /// <returns></returns>
    public float ParamRate(int a_paramId)
    {
        return GetFeaturesMultiplied(DataBaseItem.Feature.Code.ParamRate, a_paramId);
    }

    /// <summary>
    /// Get the final value of the parameter.
    /// </summary>
    /// <param name="a_paramId"></param>
    /// <returns></returns>
    public int Param(int a_paramId)
    {
        int value = ParamBase(a_paramId) + ParamSum(a_paramId); // + param plus
        value = (int)(value * ParamRate(a_paramId));
        value = Math.Max(GetParamMin(a_paramId), Math.Min(GetParamMax(a_paramId), value));
        return value;
    }

    public float XParam(int a_paramId)
    {
        return GetFeaturesSum(DataBaseItem.Feature.Code.XParam, a_paramId);
    }
    #endregion

    public bool SpecialFlag(int flagId)
    {
        return GetFeatures(DataBaseItem.Feature.Code.SpecialFlag).Any(x => x.dataId == flagId);
    }

    public bool preserveTp { get { return SpecialFlag((int)SpecialFlags.preserveTp); } }

    /// <summary>
    /// Refreshes this battler, contraining its hp and mp between the min and max, and updating the state of the Death Status.
    /// </summary>
    public virtual void Refresh()
    {
        m_currentHp = Math.Max(Math.Min(m_currentHp, mhp), 0);
        m_currentMp = Math.Max(Math.Min(m_currentMp, mmp), 0);
        m_currentTp = Math.Max(Math.Min(m_currentTp, mtp), 0);
        if (m_currentHp == 0) AddState(c_deathStateId);
        else EraseState(c_deathStateId);
    }

    /// <summary>
    /// Fully recovers this battler.
    /// </summary>
    public void RecoverAll()
    { 
        ClearStates();
        m_currentHp = mhp;
        m_currentMp = mmp;
    }

    /// <summary>
    /// Get this battler's restriction.
    /// </summary>
    public DataState.Restriction restriction
    {
        get
        {
            DataState.Restriction result = DataState.Restriction.none;
            foreach (DataState state in states) if (state.restriction > result) result = state.restriction;
            return result;
        }
    }

    /// <summary>
    /// Get the percentage of HP this battler currently has.
    /// </summary>
    public float hpPercentage { get { return (mhp > 0 ? m_currentHp * 1f / mhp : 0); } }

    /// <summary>
    /// Get the percentage of MP this battler currently has.
    /// </summary>
    public float mpPercentage { get { return (mmp > 0 ? m_currentMp * 1f / mmp : 0); } }
    
    /// <summary>
    /// Get the percentage of TP this battler currently has.
    /// </summary>
    public float tpPercentage { get { return (mtp > 0 ? m_currentTp * 1f / mtp : 0); } }

    /// <summary>
    /// Whether this battler is dead or not.
    /// </summary>
    public bool isDead { get { return HasDeathState(); } }

    /// <summary>
    /// Whether this unit is alive or not.
    /// </summary>
    public bool isAlive { get { return !HasDeathState(); } }

    /// <summary>
    /// Whether this unit
    /// </summary>
    public bool isNormal { get { return restriction == DataState.Restriction.none; } }

    /// <summary>
    /// Whether this unit can select actions.
    /// </summary>
    public bool isInputable { get { return isNormal; } } // && not auto battle

    /// <summary>
    /// Whether this unit can act.
    /// </summary>
    public bool canAct { get { return restriction < DataState.Restriction.unableToAct; } }

    /// <summary>
    /// Whether this unit can move.
    /// </summary>
    public bool isMovable { get { return true; } }

    /// <summary>
    /// Whether this unit is an actor.
    /// </summary>
    public virtual bool isActor { get { return false; } }

    /// <summary>
    /// Whether this unit is an enemy.
    /// </summary>
    public virtual bool isEnemy { get { return false; } }

    /// <summary>
    /// Clears all actions for this unit.
    /// </summary>
    public virtual void ClearActions()
    {
        m_actions.Clear();
    }

    /// <summary>
    /// Clears alls states for this unit.
    /// </summary>
    public void ClearStates()
    {
        m_states = new List<int>();
        m_stateTurns = new Dictionary<int, int>();
        m_stateStacks = new Dictionary<int, int>();
    }

    /// <summary>
    /// Adds the supplied state (or a stack if the state is already applied).
    /// </summary>
    /// <param name="a_stateId"></param>
    public void AddState(int a_stateId)
    {
        if (!IsStateAddable(a_stateId)) return;
        if (!HasState(a_stateId)) { AddNewState(a_stateId); }
        else { AddStateStack(a_stateId); } 
        ResetStateCounts(a_stateId);
        m_actionResult.addedStates.Add(a_stateId);
    }

    /// <summary>
    /// Whether the state is addable or not.
    /// </summary>
    /// <param name="a_stateId"></param>
    /// <returns></returns>
    public bool IsStateAddable(int a_stateId)
    {
        return isAlive && a_stateId < DataManager.states.Count && DataManager.states[a_stateId] != null &&
            !IsStateRemoved(a_stateId);
        // && ! state resist && !state restrict
    }

    /// <summary>
    /// If this state has been removed in this turn.
    /// </summary>
    /// <param name="a_stateId"></param>
    /// <returns></returns>
    public bool IsStateRemoved(int a_stateId)
    {
        return m_actionResult.removedStates.Contains(a_stateId);
    }

    /// <summary>
    /// Adds a new state/
    /// </summary>
    /// <param name="a_stateId"></param>
    public void AddNewState(int a_stateId)
    {
        if (a_stateId == c_deathStateId) { Die(); }
        m_states.Add(a_stateId);
        m_stateStacks.Add(a_stateId, 1);
        if (restriction > DataState.Restriction.none) OnRestrict();
    }

    /// <summary>
    /// Adds a stack to the supplied state.
    /// </summary>
    /// <param name="a_stateId"></param>
    public void AddStateStack(int a_stateId)
    {
        if (!m_stateStacks.ContainsKey(a_stateId)) { m_stateStacks.Add(a_stateId, 1); }
        else { m_stateStacks[a_stateId] = Math.Min(m_stateStacks[a_stateId] + 1, DataManager.states[a_stateId].maxStacks); }
    }

    /// <summary>
    /// Called when this battler is restricted.
    /// </summary>
    public void OnRestrict()
    {
        ClearActions();
        foreach (DataState state in states)
        {
            if (state.removeByRestriction) { RemoveState(state.id); }
        }
    }

    /// <summary>
    /// Resets the timers for the supplied state.
    /// </summary>
    /// <param name="a_stateId"></param>
    public void ResetStateCounts(int a_stateId)
    {
        DataState state = DataManager.states[a_stateId];
        int variance = Math.Max(state.maxTurns - state.minTurns, 0);
        int newTurnsCount = state.minTurns + UnityEngine.Random.Range(0, variance);
        if (m_stateTurns.ContainsKey(a_stateId)) { m_stateTurns[a_stateId] = newTurnsCount; }
        else { m_stateTurns.Add(a_stateId, newTurnsCount); }
        if (!m_stateStacks.ContainsKey(a_stateId)) { m_stateStacks.Add(a_stateId, 1); }
    }

    /// <summary>
    /// Removes the supplied state.
    /// </summary>
    /// <param name="a_stateId"></param>
    public void RemoveState(int a_stateId)
    {
        if (HasState(a_stateId))
        {
            m_stateStacks[a_stateId] -= 1;
            if (m_stateStacks[a_stateId] == 0 || !DataManager.states[a_stateId].removePerStack)
            {
                if (a_stateId == c_deathStateId) { Revive(); }
                EraseState(a_stateId);
            }
            Refresh();
            m_actionResult.removedStates.Add(a_stateId);
        }
    }

    /// <summary>
    /// Removes stacks from the supplied state.
    /// </summary>
    /// <param name="a_stateId"></param>
    /// <param name="a_amount"></param>
    public void RemoveStateStacks(int a_stateId, int a_amount)
    {
        if (a_amount == 0) RemoveState(a_stateId);
        else if (HasState(a_stateId))
        {
            if (a_amount >= m_stateStacks[a_stateId]) { RemoveState(a_stateId); }
            else
            {
                m_stateStacks[a_stateId] -= a_amount;
                ResetStateCounts(a_stateId);
                Refresh();
            }
        }
    }

    /// <summary>
    /// Sets this battler to dead.
    /// </summary>
    public void Die()
    {
        m_currentHp = 0;
        ClearStates();
    }

    /// <summary>
    /// Revives this battler.
    /// </summary>
    public void Revive()
    {
        if (m_currentHp == 0) m_currentHp = 1;
    }

    /// <summary>
    /// Updates the turn timers for all states.
    /// </summary>
    public void UpdateStateTurns()
    {
        foreach (int stateId in m_states)
        {
            m_stateTurns[stateId] -= 1;
            if (canAct)
            {
                m_actions.Add(new GameAction(this));
            }
        }
    }

    /// <summary>
    /// Removes all battle only state.
    /// </summary>
    public void RemoveBattleStates()
    {
        foreach (DataState state in states) { if (state.removeAtBattleEnd) { RemoveState(state.id); } }
    }

    /// <summary>
    /// Remove states based on their timing.
    /// </summary>
    /// <param name="a_timing"></param>
    public void RemoveStatesAuto(int a_timing)
    {
        foreach (DataState state in states)
        {
            if (m_stateTurns[state.id] == 0 && state.autoRemovalTiming == a_timing)
            {
                RemoveState(state.id);
            }
        }
    }

    /// <summary>
    /// Removes states by damage.
    /// </summary>
    /// <param name="a_damage"></param>
    public void RemoveStatesByDamage(int a_damage)
    {
        foreach (DataState state in states)
        {
            if (state.removeByDamage)
            {
                int maxValue = state.maxByDamage;
                if (maxValue == 0) maxValue = int.MaxValue;
                if (a_damage >= state.minByDamage && a_damage <= maxValue)
                {
                    if (state.chanceByDamage == 100 || UnityEngine.Random.Range(0, 100) < state.chanceByDamage)
                    {
                        RemoveState(state.id);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Get the amount of actions this battler can select.
    /// </summary>
    /// <returns></returns>
    public int MakeActionTimes()
    {
        return 1;
    }

    /// <summary>
    /// Prepare this battler's actions.
    /// </summary>
    public virtual void MakeActions()
    {
        ClearActions();
        if (!canAct) return;
        m_actions.Clear();
        for (int i = 0; i < MakeActionTimes(); i++) { m_actions.Add(new GameAction(this)); }
    }

    /// <summary>
    /// Get this battler's current (first) action.
    /// </summary>
    public GameAction currentAction { get { return m_actions.Count > 0 ? m_actions[0] : null; } }

    /// <summary>
    /// Removes this battler's current (first) action.
    /// </summary>
    public void RemoveCurrentAction()
    {
        m_actions.RemoveAt(0);
    }

    /// <summary>
    /// Get the mp cost for a skill for this battler.
    /// </summary>
    /// <param name="a_skill"></param>
    /// <returns></returns>
    public int SkillMpCost(DataSkill a_skill)
    {
        return a_skill.mpCost; // * mana cost rate
    }

    public int SkillTpCost(DataSkill a_skill)
    {
        return a_skill.tpCost; // * tp cost rate
    }

    /// <summary>
    /// Whether this battler can pay the skill cost.
    /// </summary>
    /// <param name="a_skill"></param>
    /// <returns></returns>
    public bool IsSkillCostPayable(DataSkill a_skill)
    {
        foreach (DataSkill.StateCost stateCost in a_skill.statesCost)
        {
            int stateId = DataManager.GetIndex(stateCost.state);
            if (!HasState(stateId) || StateStacks(stateId) < stateCost.stacks) return false;
        }
        return currentMp >= SkillMpCost(a_skill) && currentTp >= SkillTpCost(a_skill);
    }

    public void PaySkillCost(DataSkill a_skill)
    {
        foreach (DataSkill.StateCost stateCost in a_skill.statesCost)
        {
            RemoveStateStacks(DataManager.GetIndex(stateCost.state), stateCost.stacks);
        }
        currentMp -= SkillMpCost(a_skill);
        currentTp -= SkillTpCost(a_skill);
    }

    public bool IsOccasionOk(DataUsableItem a_item)
    {
        return true;//(GameParty.instance.isInBattle ? a_item.isBattleOk : a_item.isMenuOk); // TODO: temporarily disabled. Skill buttons interactibility needs to be refreshed and item occassions need to be checked
    }

    public bool AreUsableItemCondtionsMet(DataUsableItem a_item)
    {
        return canAct && IsOccasionOk(a_item);
    }

    public bool AreSkillConditionsMet(DataSkill a_skill)
    {
        return AreUsableItemCondtionsMet(a_skill) && IsSkillCostPayable(a_skill); // skill wtype okay and skill not sealed and type not sealed
    }

    // TODO improve function
    public bool IsUsable(DataUsableItem a_item)
    {
        if (a_item == null) return false;
        if (a_item.isSkill) return AreSkillConditionsMet(a_item as DataSkill);
        if (a_item.isItem) return true;
        return false;
    }

    public int MakeDamageValue(GameBattler a_user, DataUsableItem a_item)
    {
        int value = a_user.atk * 2 - def;
        // value = item.damage
        // value *= element rate
        // value *= pdr, mdr
        // value *= rec if recover?
        // criticals
        // variance
        // guard
        // other effects
        return value;
    }

    public void ExecuteDamage(GameBattler a_user)
    {
        if (m_actionResult.hpDamage > 0) { OnDamage(m_actionResult.hpDamage); }
        currentHp -= m_actionResult.hpDamage;
        currentMp -= m_actionResult.mpDamage;
        currentHp += m_actionResult.hpDrain;
        currentMp += m_actionResult.mpDrain;
    }

    public void UseItem(DataUsableItem a_item)
    {
        if (a_item.isSkill) PaySkillCost(a_item as DataSkill);
        if (a_item.isItem) ConsumeItem(a_item);
    }

    public void ConsumeItem(DataUsableItem a_item)
    {
        throw new NotImplementedException();
    }

    public float ItemHit(GameBattler a_user, DataUsableItem a_item)
    {
        float rate = a_item.successRate;
        if (a_item.isPhysical) rate *= a_user.hit;
        //if (item.isMagical && useHitForMagic) rate *= user.hit;
        return rate;
    }

    public float ItemEva(GameBattler a_user, DataUsableItem a_item)
    {
        if (a_item.isPhysical) return eva;
        if (a_item.isMagical) return mev;
        return 0f;
    }

    public void ItemApply(GameBattler a_user, DataUsableItem a_item)
    {
        m_actionResult.Clear();
        m_actionResult.SetItem(a_user, a_item);
        float hitRate = ItemHit(a_user, a_item) - ItemEva(a_user, a_item);
        m_actionResult.missed = UnityEngine.Random.value > hitRate;
        if (m_actionResult.isHit)
        {
            // check if is critical if everything can crit
            if (!a_item.damage.isNone)
            {
                // check if is critical
                m_actionResult.MakeDamageValue(MakeDamageValue(a_user, a_item), a_item);
                ExecuteDamage(a_user);
            }
            foreach (DataUsableItem.Effect effect in a_item.effects)
            {
                ItemEffectApply(a_user, a_item, effect);
            }
            ItemUserEffect(a_user, a_item);
        }
    }

    public void ItemEffectApply(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        switch(a_effect.code)
        {
            case DataUsableItem.Effect.EffectType.RecoverHP: ItemEffectRecoverHP(a_user, a_item, a_effect); break;
            case DataUsableItem.Effect.EffectType.RecoverMP: ItemEffectRecoverMP(a_user, a_item, a_effect); break;
            case DataUsableItem.Effect.EffectType.AddState: ItemEffectAddState(a_user, a_item, a_effect); break;
            case DataUsableItem.Effect.EffectType.RemoveState: ItemEffectRemoveState(a_user, a_item, a_effect); break;
            case DataUsableItem.Effect.EffectType.Displace: ItemEffectDisplace(a_user, a_item, a_effect); break;
        }
    }

    public void ItemEffectRecoverHP(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        int value = (int)(mhp * a_effect.value1 + a_effect.value2);
        // some more stuff normally happens
        m_actionResult.hpRecovery += value;
        currentHp += value;
    }

    public void ItemEffectRecoverMP(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        int value = (int)(mhp * a_effect.value1 + a_effect.value2);
        throw new NotImplementedException();

        m_actionResult.mpRecovery += value;
        currentMp += value;
    }

    public void ItemEffectAddState(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        if (a_effect.valueData == null) { ItemEffectAddStateAttack(a_user, a_item, a_effect); }
        else { ItemEffectAddStateNormal(a_user, a_item, a_effect); }
    }

    public List<int> attackStates
    {
        get
        { return GetFeaturesSetUnique(DataBaseItem.Feature.Code.AttackState); }
    }

    public void ItemEffectAddStateAttack(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        foreach(int stateId in attackStates)
        {
            float chance = a_effect.value1;
            //chance *= state rate
            //chance *= user attack state rate
            //chance *= luk effect rate
            if (UnityEngine.Random.value < chance) { AddState(stateId); }
        }
    }

    public void ItemEffectAddStateNormal(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        if (a_effect.valueData.isState)
        {
            float chance = a_effect.value1;
            //chance *= state rate
            //chance *= luk effect rate
            if (UnityEngine.Random.value < chance) { AddState(DataManager.GetIndex(a_effect.valueData as DataState)); }
        }
    }

    public void ItemEffectRemoveState(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        if (a_effect.valueData.isState)
        {
            float chance = a_effect.value1;
        if (UnityEngine.Random.value < chance) { RemoveState(DataManager.GetIndex(a_effect.valueData as DataState)); }
        }
    }

    public void ItemEffectDisplace(GameBattler a_user, DataUsableItem a_item, DataUsableItem.Effect a_effect)
    {
        int value = (int)a_effect.value1;
        switch (a_effect.dataId)
        {
            // Move Target
            case 0:
                friendsUnit.MovePosition(position, value);
                break;
            // Move User
            case 1:
                a_user.friendsUnit.MovePosition(a_user.position, value);
                break;
            // Target Random Displace
            case 2:
                friendsUnit.SwapPosition(position, UnityEngine.Random.Range(-5, 5));
                break;
            // Target Random Swap
            case 3:
                friendsUnit.SwapPosition(position, UnityEngine.Random.Range(0, friendsUnit.slots.Length));
                break;
            // User Random Displace
            case 4:
                a_user.friendsUnit.SwapPosition(a_user.position, UnityEngine.Random.Range(-5, 5));
                break;
            // User Random Swap
            case 5:
                a_user.friendsUnit.SwapPosition(a_user.position, UnityEngine.Random.Range(0, a_user.friendsUnit.slots.Length));
                break;
            // Swap User with Target
            case 6:
                // TODO: check if user is friend
                friendsUnit.MovePosition(position, a_user.position);
                break;
        }
    }

    public void ItemUserEffect(GameBattler a_user, DataUsableItem a_item)
    {

    }

    public void OnActionStart()
    {
        currentAction.Prepare();
    }

    public void OnActionEnd()
    {
        // clear result
        RemoveStatesAuto(1);
    }

    public void OnTurnEnd()
    {
        // clear result
        UpdateStateTurns();
        RemoveStatesAuto(2);
    }

    public void OnBattleStart()
    {
        if (!preserveTp) { InitTp(); }
    }

    public void OnBattleEnd()
    {
        RemoveBattleStates();
        if (!preserveTp) { ClearTp(); }
    }

    public void OnDamage(int a_damage)
    {
        RemoveStatesByDamage(a_damage);
    }

    public virtual GameUnit friendsUnit
    {
        get { return new GameUnit(); }
    }

    public virtual GameUnit opponentsUnit
    {
        get { return new GameUnit(); }
    }

    public List<DataUsableItem> onHitItems
    {
        get
        {
            List<DataBaseItem> onHitItems = GetFeaturesData(DataBaseItem.Feature.Code.OnHitSkill);
            List<DataUsableItem> items = new List<DataUsableItem>();
            foreach(DataBaseItem item in onHitItems)
            {
                if (item.isUsableItem)
                {
                    items.Add(item as DataUsableItem);
                }
            }
            return items;
        }
    }

    public bool IsOpposite(GameBattler a_battler)
    {
        return isActor != a_battler.isActor;
    }

    public bool IsFriend (GameBattler a_battler)
    {
        return !IsOpposite(a_battler);
    }

    public void InitTp()
    {
        currentTp = UnityEngine.Random.Range(0, 25);
    }

    public void ClearTp()
    {
        currentTp = 0;
    }
}
