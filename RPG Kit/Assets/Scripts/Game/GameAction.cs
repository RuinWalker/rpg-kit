﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameAction
{
    public GameBattler subject { get { return m_subject; } } 
    private GameBattler m_subject;
    private bool m_forcing;
    public DataUsableItem item { get { return m_item; } }
    public List<int> targetIndices { get { return m_targetIndices; } }

    private DataUsableItem m_item;
    private List<int> m_targetIndices;
    public List<GameBattler> targets { get { return m_targets; } }
    private List<GameBattler> m_targets;

    public GameAction(GameBattler a_subject, bool a_forcing = false)
    {
        m_subject = a_subject;
        m_forcing = a_forcing;
        m_targetIndices = new List<int>();
    }

    public GameAction(GameBattler a_subject, DataUsableItem a_item, bool a_forcing = false)
    {
        m_subject = a_subject;
        m_item = a_item;
        m_forcing = a_forcing;
        m_targetIndices = new List<int>();
    }

    public void Clear()
    {
        m_item = null;
        m_targetIndices.Clear();
    }

    public GameUnit friendsUnit { get { return m_subject.friendsUnit; } }
    public GameUnit opponentsUnit { get { return m_subject.opponentsUnit; } }

    public void SetSkill(int a_skillId)
    {
        m_item = DataManager.skills[a_skillId];
    }

    public void SetItem(int a_itemId)
    {
        m_item = DataManager.skills[a_itemId];
    }

    public void SetObject(DataUsableItem a_item)
    {
        m_item = a_item;
    }

    public void Prepare()
    {
        // set confusion
    }

    // TODO not right
    public bool isValid
    {
        get
        {
            return (m_forcing && item != null) || m_subject.IsUsable(item);
        }
    }

    public List<GameBattler> MakeTargets()
    {
        // confusion stuff
        if (item.isForUser) { m_targets = userTarget; }
        else if (item.isForOpponent) { m_targets = TargetsForOpponents(); }
        else if (item.isForFriend) { m_targets = TargetsForFriends(); }
        else { m_targets = emptyTargets; }
        return m_targets;
    }

    public List<GameBattler> TargetsForOpponents()
    {
        if (item.isForAll) { return GetAllTargets(m_subject.opponentsUnit); }
        else { return GetTargetsByIndices(m_subject.opponentsUnit); }
    }

    public List<GameBattler> TargetsForFriends()
    {
        if (item.isForDeadFriend)
        {
            return emptyTargets; // TODO: DEAD FRIENDS
        }
        else if (item.isForOne) { return GetTargetsByIndices(m_subject.friendsUnit); }
        else { return GetAllTargets(m_subject.friendsUnit); }
    }

    public List<GameBattler> GetTargetsByIndices(GameUnit a_unit)
    {
        return GetTargetsByIndices(m_targetIndices, a_unit);
    }

    public List<GameBattler> GetTargetsByIndices(List<int> a_targetIndices, GameUnit a_unit)
    {
        List<GameBattler> result = new List<GameBattler>();
        foreach (int index in a_targetIndices) { if (a_unit.slots[index] != null) { result.Add(a_unit.slots[index]); } }
        return result;
    }

    public List<GameBattler> GetAllTargets(GameUnit a_unit)
    {
        List<int> _targetIndices = new List<int>();
        for (int i = 0; i < item.range.Length; i++)
        {
            if (item.range[i]) _targetIndices.Add(i);
        }
        return GetTargetsByIndices(_targetIndices, a_unit);
    }

    public int unselectedTargetsCount { get { return item.targets - m_targetIndices.Count; } }

    public List<GameBattler> MakeFollowUpTargets(GameAction a_rootAction, List<GameBattler> a_rootTargets)
    {
        if (item.isForUser) { m_targets = userTarget; }
        else if (item.isForOpponent) { m_targets = FollowUpTargetsForOpponents(a_rootAction, a_rootTargets); }
        else if (item.isForFriend) { m_targets = FollowUpTargetsForFriends(a_rootAction, a_rootTargets); }
        else { m_targets = emptyTargets; }
        return m_targets;
    }

    public List<GameBattler> FollowUpTargetsForOpponents(GameAction a_rootAction, List<GameBattler> a_rootTargets)
    {
        if (item.isForAllFollowUp)
        {
            if (a_rootAction.item.isForOpponent) { return a_rootTargets; }
            else { return emptyTargets; }
        }
        else if (item.isForAll) { return GetAllTargets(m_subject.opponentsUnit); }
        else
        {
            if (a_rootAction.item.isForOpponent)
            {
                List<GameBattler> result = new List<GameBattler>();
                result.AddRange(a_rootTargets.GetRange(0, 100)); //System.Math.Min(item.targets, a_rootTargets.Count)
                return result;
            }
            else { return emptyTargets; }
        }
    }

    public List<GameBattler> FollowUpTargetsForFriends(GameAction a_rootAction, List<GameBattler> a_rootTargets)
    {
        if (item.isForAllFollowUp)
        {
            if (a_rootAction.item.isForFriend) { return a_rootTargets; }
            else { return emptyTargets; }
        }
        else if (item.isForDeadFriend)
        {
            return emptyTargets; // TODO: DEAD FRIENDS
        }
        else if (item.isForAll) { return GetAllTargets(m_subject.friendsUnit); }
        else
        {
            if (a_rootAction.item.isForFriend)
            {
                List<GameBattler> result = new List<GameBattler>();
                result.AddRange(a_rootTargets.GetRange(0, 100)); //System.Math.Min(item.targets, a_rootTargets.Count)
                return result;
            }
            else { return emptyTargets; }
        }
    }

    public List<GameBattler> MakeOnHitTargets(GameAction a_rootAction, GameBattler a_rootTarget)
    {
        m_targets = emptyTargets;
        if (item.isForUser) { m_targets = userTarget; }
        else if (item.isForOpponent && a_rootTarget.IsOpposite(m_subject)) { m_targets.Add(a_rootTarget); }
        else if (item.isForFriend && a_rootTarget.IsFriend(m_subject)) { m_targets.Add(a_rootTarget); }
        return m_targets;
    }

    public List<GameBattler> userTarget { get { return new List<GameBattler>(new GameBattler[] { m_subject }); } }
    static public List<GameBattler> emptyTargets { get { return new List<GameBattler>(); } }
}
