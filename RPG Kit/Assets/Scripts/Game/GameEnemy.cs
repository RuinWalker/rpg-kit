﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEnemy : GameBattler {

    private int m_enemyId;

    public override Sprite battlerSprite { get { return enemy.battlerSprite; } }

    public GameEnemy(int a_enemyId) : base()
    {
        m_enemyId = a_enemyId;
        battlerName = enemy.displayName;

        currentHp = mhp;
        currentMp = mmp;
    }

    public DataEnemy enemy { get { return DataManager.enemies[m_enemyId]; } }

    public override bool isEnemy { get { return true; } }

    public override GameUnit friendsUnit
    {
        get
        {
            return GameTroop.instance;
        }
    }

    public override GameUnit opponentsUnit
    {
        get
        {
            return GameParty.instance;
        }
    }

    public override List<DataBaseItem> featureObjects
    {
        get
        {
            List<DataBaseItem> result = base.featureObjects;
            result.Add(enemy);
            return result;
        }
    }

    public override int ParamBase(int a_paramId)
    {
        return enemy.parameters[a_paramId];
    }

    public int exp { get { return enemy.exp; } }
    public int gold { get { return enemy.gold; } }

    public override void MakeActions()
    {
        base.MakeActions();
        if (actions.Count == 0) { return; }

        List<DataEnemy.Action> actionList = new List<DataEnemy.Action>();
        foreach (DataEnemy.Action action in enemy.actions)
        {
            // if valid
            actionList.Add(action);
        }
        if (actionList.Count == 0) { return; }
        // do some more ai stuff

        actions[0].SetObject(actionList[0].item);
        actions[0].targetIndices.Add(0);
    }
}
