﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupText : MonoBehaviour {

    public float lifeTime { get { return m_lifeTime; } set { m_lifeTime = value; } }
    public float fadeTime { get { return m_fadeTime; } set { m_fadeTime = value; } }

    [SerializeField] private float m_lifeTime;
    [SerializeField] private float m_fadeTime;
    private float m_birthTime;
    [SerializeField] private float m_speed;
    private Coroutine m_deathRoutine;

    // Use this for initialization
    void Start () {
        m_birthTime = Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<RectTransform>().anchoredPosition += Vector2.up * m_speed;
        if (m_deathRoutine == null && Time.time - m_birthTime > m_lifeTime)
        {
            m_deathRoutine = StartCoroutine(StartDeath());
        }
	}

    IEnumerator StartDeath()
    {
        Text comp = GetComponent<Text>();
        Color color = comp.color;
        float fadeSpeed = 1 / fadeTime;
        while (color.a > 0)
        {
            color.a = Mathf.MoveTowards(color.a, 0, fadeSpeed * Time.deltaTime);
            comp.color = color;
            yield return new WaitForEndOfFrame();
        }
        Destroy(gameObject);
    }
}
