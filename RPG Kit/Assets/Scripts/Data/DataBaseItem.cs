﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DataBaseItem : ScriptableObject {

    [System.Serializable]
    public struct Feature
    {
        public enum Code { Param, XParam, SParam, ParamRate, OnHitSkill, AttackState, SpecialFlag };

        public Code code { get { return m_code; } }
        public int dataId { get { return m_dataId; } }
        public float value { get { return m_value; } }
        public DataBaseItem valueData { get { return m_valueData; } }

        [SerializeField] private Code m_code;
        [SerializeField] private int m_dataId;
        [SerializeField] private float m_value;
        [SerializeField] private DataBaseItem m_valueData;
    }

    public int id { get { return m_id; } }
    public string displayName { get { return m_displayName; } }
    public Sprite icon { get { return m_icon; } }
    public Feature[] features { get { return m_features; } }
    public string note { get { return m_note; } }

    private int m_id;
    [SerializeField] private string m_displayName;
    [SerializeField] private Sprite m_icon;
    [SerializeField] private Feature[] m_features;
    [SerializeField] private string m_note;

    public void SetId(int a_id) { m_id = a_id; } // function so that id is not mistaken to be used for assigning. Should only be used in DataManager.

    public virtual bool isUsableItem { get { return false; } }
    public virtual bool isSkill { get { return false; } }
    public virtual bool isItem { get { return false; } }
    public virtual bool isEquip { get { return false; } }
    public virtual bool isWeapon{ get { return false; } }
    public virtual bool isArmor { get { return false; } }
    public virtual bool isState { get { return false; } }
    public virtual bool isClass{ get { return false; } }
    public virtual bool isActor { get { return false; } }
    public virtual bool isEnemy { get { return false; } }
    
}
