﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public class DataUsableItem : DataBaseItem
{
    public enum Scope { none, oneEnemy, allEnemies, allEnemiesFollowUp, oneAlly, allAllies, allAlliesFollowUp, oneDeadAlly, allDeadAllies, allDeadAlliesFollowUp, user }
    public enum Occassion { never, always, battle, menu}
    public enum HitType { guaranteed, physical, magical }

    [System.Serializable]
    public class Damage
    {
        public enum Type { none, hpDamage, mpDamage, hpRecover, mpRecover, hpDrain, mpDrain }
        public Type type { get { return m_type; } }
        public float drainValue { get { return m_drainValue; } }
        public int elementId { get { return m_elementId; } }
        public float variance { get { return m_variance; } }
        public bool canCritical { get { return m_canCritical; } }

        public bool isNone { get { return type == 0; } }
        public bool isToHp { get { return new List<Type>(new Type[] { Type.hpDamage, Type.hpRecover, Type.hpDrain }).Contains(m_type); } }
        public bool isToMp { get { return new List<Type>(new Type[] { Type.mpDamage, Type.mpRecover, Type.mpDrain }).Contains(m_type); } }
        public bool isRecover { get { return new List<Type>(new Type[] { Type.hpRecover, Type.mpRecover }).Contains(m_type); } }
        public bool isDrain { get { return new List<Type>(new Type[] { Type.hpDrain, Type.mpDrain }).Contains(m_type); } }
        public int sign { get { return isRecover ? -1 : 1; } }

        [SerializeField] private Type m_type;
        [SerializeField] private float m_drainValue;
        [SerializeField] private int m_elementId;
        [SerializeField] private float m_variance;
        [SerializeField] private bool m_canCritical;
    }

    [System.Serializable]
    public struct Effect
    {
        public enum EffectType { RecoverHP, RecoverMP, AddState, RemoveState, Displace }
        public EffectType code { get { return m_code; } }
        public int dataId { get { return m_data; } }
        public float value1 { get { return m_value1; } }
        public float value2 { get { return m_value2; } }
        public DataBaseItem valueData { get { return m_valueData; } }
        [SerializeField] private EffectType m_code;
        [SerializeField] private int m_data;
        [SerializeField] private float m_value1;
        [SerializeField] private float m_value2;
        [SerializeField] private DataBaseItem m_valueData;
    }

    public Scope scope { get { return m_scope; } }
    public Occassion occassion { get { return m_occassion; } }
    public int priority { get { return m_priority; } }
    public int targets { get { return m_targets; } }
    public bool[] range { get { return m_range; } }
    public bool[] usableRange { get { return m_usableRange; } }
    public float successRate { get { return m_successRate; } }
    public float repeats { get { return m_repeats > 0 ? m_repeats : 1; } }
    public HitType hitType { get { return m_hitType; } }
    public Damage damage { get { return m_damage; } }
    public Effect[] effects { get { return m_effects; } }
    public DataUsableItem[] followUpItems { get { return m_followUpItems; } }

    [SerializeField] private Scope m_scope;
    [SerializeField] private Occassion m_occassion;
    [SerializeField] private int m_priority;
    [SerializeField] private int m_targets;
    [SerializeField] private bool[] m_range;
    [SerializeField] private bool[] m_usableRange;
    [SerializeField] [Range(0f, 1f)] private float m_successRate;
    [SerializeField] [Range(0, 999)] private int m_repeats;
    [SerializeField] private HitType m_hitType;
    [SerializeField] private Damage m_damage;
    [SerializeField] private Effect[] m_effects;
    [SerializeField] private DataUsableItem[] m_followUpItems;

    public bool isForOpponent { get { return new List<Scope>(new Scope[] { Scope.oneEnemy, Scope.allEnemies, Scope.allEnemiesFollowUp }).Contains(m_scope); } }
    public bool isForFriend { get { return new List<Scope>(new Scope[] { Scope.oneAlly, Scope.allAllies, Scope.oneDeadAlly, Scope.allDeadAllies, Scope.user, Scope.allAlliesFollowUp, Scope.allDeadAlliesFollowUp }).Contains(m_scope); } }
    public bool isForDeadFriend { get { return new List<Scope>(new Scope[] { Scope.oneDeadAlly, Scope.allDeadAllies, Scope.allDeadAlliesFollowUp }).Contains(m_scope); } }
    public bool isForUser { get { return m_scope == Scope.user; } }
    public bool isForOne { get { return new List<Scope>(new Scope[] { Scope.oneEnemy, Scope.oneAlly, Scope.oneDeadAlly, Scope.user }).Contains(m_scope); } }
    public bool isForAll { get { return new List<Scope>(new Scope[] { Scope.allEnemies, Scope.allAllies, Scope.allDeadAllies, Scope.allEnemiesFollowUp, Scope.allAlliesFollowUp, Scope.allDeadAlliesFollowUp }).Contains(m_scope); } }
    public bool isForAllFollowUp { get { return new List<Scope>(new Scope[] { Scope.allEnemiesFollowUp, Scope.allAlliesFollowUp, Scope.allDeadAlliesFollowUp }).Contains(m_scope); } }
    public bool needSelection { get { return new List<Scope>(new Scope[] { Scope.oneEnemy, Scope.oneAlly, Scope.oneDeadAlly }).Contains(m_scope); } }
    public bool isCertain { get { return m_hitType == HitType.guaranteed; } }
    public bool isPhysical { get { return m_hitType == HitType.physical; } }
    public bool isMagical { get { return m_hitType == HitType.magical; } }
    public bool isBattleOk { get { return m_occassion == Occassion.always || m_occassion == Occassion.battle; } }
    public bool isMenuOk { get { return m_occassion == Occassion.always || m_occassion == Occassion.menu; } }
    public bool isPassive { get { return m_occassion == Occassion.never; } }

    public override bool isUsableItem { get { return true; } }
}
