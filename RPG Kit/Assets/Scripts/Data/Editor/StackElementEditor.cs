﻿#if UNITY_EDITO
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Linq;

[CustomEditor(typeof(StackElement))]
public class StackElementEditor : Editor {

    #region Variables
    #region Serialized Properties
    protected SerializedProperty m_useOrientatedContent;
    protected SerializedProperty m_portraitContent;
    protected SerializedProperty m_landscapeContent;
    
    /// <summary>
    /// Show animation params of the element. Parent of all the other animation params properties.
    /// </summary>
    protected SerializedProperty m_showAnimations;
    /// <summary>
    /// Hide animation params of the element. Parent of all the other animation params properties.
    /// </summary>
    protected SerializedProperty m_hideAnimations;

    protected SerializedProperty m_boolParams;
    protected SerializedProperty m_intParams;
    protected SerializedProperty m_floatParams;
    protected SerializedProperty m_vectorParams;
    protected SerializedProperty m_stringParams;
    #endregion

    #region UI Variables
    /// <summary>
    /// The currently select animations list, based on the selected tab.
    /// </summary>
    protected ReorderableList currentList
    {
        get
        {
            if (serializedObject.FindProperty("m_hasHideAnimation").boolValue && m_selectedTabIndex == 1) return m_hideAnimationsList;
            else return m_showAnimationsList;
        }
    }

    /// <summary>
    /// The currently selected animations property, based on the selected tab.
    /// </summary>
    protected SerializedProperty currentProperty
    {
        get
        {
            if (serializedObject.FindProperty("m_hasHideAnimation").boolValue && m_selectedTabIndex == 1) return m_hideAnimations;
            else return m_showAnimations;
        }
    }

    /// <summary>
    /// The list containing all show animations for this stack element.
    /// </summary>
    protected ReorderableList m_showAnimationsList;
    /// <summary>
    /// The list containing all hide animations for this stack element.
    /// </summary>
    protected ReorderableList m_hideAnimationsList;
    /// <summary>
    /// Index of the last selected animation.
    /// </summary>
    protected int m_previousAnimIndex;

    /// <summary>
    /// The index of the currently selected animations tab (0 is show, 1 is hide).
    /// </summary>
    protected int m_selectedTabIndex;
    /// <summary>
    /// The index of the previously selected animations tab (0 is show, 1 is hide).
    /// </summary>
    protected int m_previousTabIndex;

    /// <summary>
    /// All reorderable list ui elements for each of the animation properties.
    /// </summary>
    protected List<ReorderableList> m_parameterLists;
    /// <summary>
    /// List of flags whether the list with the respective index is currently open or not.
    /// </summary>
    protected List<bool> m_isListOpenFlags;

    /// <summary>
    /// Flag whether the animation properties foldout is currently open or not.
    /// </summary>
    protected bool m_animParamsOpen;
    
    

    /// <summary>
    /// List header texts for all animation parameters lists.
    /// </summary>
    readonly string[] c_listTitles = new string[] { "Boolean Parameters", "Integer Parameters", "Float Parameters", "Vector Parameters", "String Parameters" };
    #endregion
    #endregion

    #region Methods
    #region Initialization
    // Use this for initialization
    protected virtual void OnEnable()
    {
        InitializeScreenOrientationProperties();
        InitializeAnimationProperties();
        InitializeLists();
    }

    /// <summary>
    /// Initializes all member variables for the serialized properties.
    /// </summary>
    protected virtual void InitializeScreenOrientationProperties()
    {
        m_useOrientatedContent = serializedObject.FindProperty("m_screenOrientatedContent");
        m_portraitContent = serializedObject.FindProperty("m_portraitContent");
        m_landscapeContent = serializedObject.FindProperty("m_landscapeContent");
    }

    /// <summary>
    /// Initializes all member variables for the serialized properties.
    /// </summary>
    protected virtual void InitializeAnimationProperties()
    {
        m_previousAnimIndex = -1;
        m_previousTabIndex = -1;
        m_showAnimations = serializedObject.FindProperty("m_showAnimations");
        m_hideAnimations = serializedObject.FindProperty("m_hideAnimations");
    }

    /// <summary>
    /// Create lists for all parameters.
    /// <para>Lists are reorderable due to the adaptive nature of the parameters.</para>
    /// </summary>
    protected virtual void InitializeLists()
    {
        m_isListOpenFlags = new List<bool>();
        m_parameterLists = new List<ReorderableList>();

        m_isListOpenFlags = Enumerable.Repeat(false, 5).ToList();

        m_showAnimationsList = CreateReorderableList(m_showAnimations);
        m_hideAnimationsList = CreateReorderableList(m_hideAnimations);
    }

    /// <summary>
    /// Creates a reorderable list for the given parameter and adds it to m_parameterLists. Also adds a bool entry to m_isListOpenFlags for this list.
    /// </summary>
    /// <param name="propertyForList">Property to create the list for.</param>
    protected virtual void CreateParameterList(SerializedProperty propertyForList)
    {
        m_parameterLists.Add(CreateReorderableList(propertyForList));
        while (m_isListOpenFlags.Count < m_parameterLists.Count) m_isListOpenFlags.Add(false);
    }

    /// <summary>
    /// Swaps the existing list for a newly created reordable list. If a list doesn't exist yet at the given index, adds a new list to the array (not necessarily at the given index)
    /// </summary>
    /// <param name="index"></param>
    /// <param name="propertyForList"></param>
    protected void RecreateParameterList(int index, SerializedProperty propertyForList)
    {
        if (m_parameterLists.Count > index) m_parameterLists[index] = CreateReorderableList(propertyForList);
        else CreateParameterList(propertyForList);
    }

    /// <summary>
    /// Creates a reorderable list for the given property
    /// </summary>
    /// <param name="propertyForList">Property to create the list for.</param>
    /// <returns>Created list element.</returns>
    protected virtual ReorderableList CreateReorderableList(SerializedProperty propertyForList)
    {
        ReorderableList list = new ReorderableList(serializedObject, propertyForList, true, false, true, true);
        list.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) => {
            var element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            string label = "Element " + index.ToString();
            // Draw element field type based on property type.
            switch (element.type)
            {
                case "bool": element.boolValue = EditorGUI.Toggle(rect, label, element.boolValue); break;
                case "int": element.intValue = EditorGUI.IntField(rect, label, element.intValue); break;
                case "float": element.floatValue = EditorGUI.FloatField(rect, label, element.floatValue); break;
                case "Vector3": element.vector3Value = EditorGUI.Vector3Field(rect, label, element.vector3Value); break;
                case "string": element.stringValue = EditorGUI.TextField(rect, label, element.stringValue); break;
                case "StackElementAnimParams":
                    EditorGUI.LabelField(rect, ((StackElement.StackAnimation)(element.FindPropertyRelative("transitionType").enumValueIndex)).ToString());
                    break;
                default:
                    EditorGUI.LabelField(rect, "This type has no case yet. Type: " + element.type);
                    break;
            }
        };
        return list;
    }
    #endregion

    public override void OnInspectorGUI()
    {
        // Draw all base properties
        base.OnInspectorGUI();
        // Space the animation properties from the other properties for layout purposes.
        EditorGUILayout.Space();

        // Draw all Stack Element properties
        DrawStackElementProperties();

        // Apply all changes made
        serializedObject.ApplyModifiedProperties();
    }

    /// <summary>
    /// Shorthand for drawing all stack element properties.
    /// </summary>
    protected virtual void DrawStackElementProperties()
    {
        DrawScreenOrientatedProperties();
        EditorGUILayout.Space();
        DrawAnimParams();
    }

    /// <summary>
    /// Draws all properties related to screen orientated content.
    /// </summary>
    protected virtual void DrawScreenOrientatedProperties()
    {
        /*
        DISABLED UNTIL FUTURE VERSION, EXPERIMENTAL FEATURE

        EditorGUILayout.PropertyField(m_useOrientatedContent);
        if (m_useOrientatedContent.boolValue)
        {
            EditorGUI.indentLevel++;
            EditorGUILayout.PropertyField(m_portraitContent);
            EditorGUILayout.PropertyField(m_landscapeContent);
            EditorGUI.indentLevel--;
        }
        */
    }

    #region Draw Animation Parameters
    /// <summary>
    /// Updates all animations poroperties for the given animation.
    /// </summary>
    /// <param name="animationProperty">Property holding information of an animation.</param>
    protected void UpdateProperties(SerializedProperty animationProperty)
    {
        if (m_previousAnimIndex != currentList.index || m_previousTabIndex != m_selectedTabIndex)
        {
            m_boolParams = animationProperty.FindPropertyRelative("boolParams");
            m_intParams = animationProperty.FindPropertyRelative("intParams");
            m_floatParams = animationProperty.FindPropertyRelative("floatParams");
            m_vectorParams = animationProperty.FindPropertyRelative("vectorParams");
            m_stringParams = animationProperty.FindPropertyRelative("stringParams");

            RecreateParameterList(0, m_boolParams);
            RecreateParameterList(1, m_intParams);
            RecreateParameterList(2, m_floatParams);
            RecreateParameterList(3, m_vectorParams);
            RecreateParameterList(4, m_stringParams);

            m_previousAnimIndex = currentList.index;
            m_previousTabIndex = m_selectedTabIndex;
        }
    }

    /// <summary>
    /// Draws all animation parameters for this element.
    /// </summary>
    protected void DrawAnimParams()
    {
        m_animParamsOpen = EditorGUILayout.Foldout(m_animParamsOpen, "Transition Animation Parameters");
        if (m_animParamsOpen)
        {
            EditorGUI.indentLevel++;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_hasHideAnimation"));
            if (serializedObject.FindProperty("m_hasHideAnimation").boolValue)
            {
                m_selectedTabIndex = GUILayout.Toolbar(m_selectedTabIndex, new string[] { "Show", "Hide" });
                switch (m_selectedTabIndex)
                {
                    default: break;
                }
            }
            currentList.DoLayoutList();
            
            // Check if animation selected
            if (currentList.index >= 0)
            {
                SerializedProperty element = currentProperty.GetArrayElementAtIndex(currentList.index);

                UpdateProperties(element);

                DrawCommonProperties(element);
                EditorGUILayout.Space();
                DrawAnimPropertiesForType(element);
            }
            EditorGUI.indentLevel--;
        }
    }

    /// <summary>
    /// Draws all animation properties common for each animation.
    /// </summary>
    protected virtual void DrawCommonProperties(SerializedProperty animationProperty)
    {
        DrawTypeField(animationProperty);
        DrawTransitionTimeField(animationProperty);
        DrawTimeTillStartField(animationProperty);
        EditorGUILayout.PropertyField(animationProperty.FindPropertyRelative("target"));
    }

    /// <summary>
    /// Draws the Transition Type property field.
    /// </summary>
    protected virtual void DrawTypeField(SerializedProperty animationProperty)
    {
        EditorGUILayout.PropertyField(animationProperty.FindPropertyRelative("transitionType"));
    }

    /// <summary>
    /// Draw the Transition Time property field.
    /// </summary>
    protected virtual void DrawTransitionTimeField(SerializedProperty animationProperty)
    {
        DrawFloatField(animationProperty, "transitionTime", "Transition Time");
    }

    /// <summary>
    /// Draw the Time Till Start property field.
    /// </summary>
    /// <param name="animationProperty"></param>
    protected virtual void DrawTimeTillStartField(SerializedProperty animationProperty)
    {
        DrawFloatField(animationProperty, "timeTillStart", "Time Till Start");
    }
    
    /// <summary>
    /// General function for drawing a float field.
    /// </summary>
    /// <param name="animationProperty"></param>
    /// <param name="propertyName">Name of the property this field affects.</param>
    /// <param name="fieldLabel">Label to show in front of the property.</param>
    protected virtual void DrawFloatField(SerializedProperty animationProperty, string propertyName, string fieldLabel)
    {
        SerializedProperty relProperty = animationProperty.FindPropertyRelative(propertyName);
        relProperty.floatValue = EditorGUILayout.FloatField(fieldLabel, relProperty.floatValue);
        relProperty.floatValue = Mathf.Max(relProperty.floatValue, 0);
    }

    
    /// <summary>
    /// Draws property fields based on the given animation type.
    /// </summary>
    /// <param name="type">Animation type to draw properties for.</param>
    protected virtual void DrawAnimPropertiesForType(SerializedProperty animationProperty)
    {
        switch ((StackElement.StackAnimation)animationProperty.FindPropertyRelative("transitionType").enumValueIndex)
        {
            case StackElement.StackAnimation.Custom:
                DrawAnimPropertiesCustom(animationProperty);
                break;
            case StackElement.StackAnimation.Slide:
                DrawAnimPropertiesSlide(animationProperty);
                break;
            case StackElement.StackAnimation.Translate:
                DrawAnimPropertiesTranslate(animationProperty);
                break;
            case StackElement.StackAnimation.Scale:
                DrawAnimPropertiesScale(animationProperty);
                break;
            case StackElement.StackAnimation.Rotate:
                DrawAnimPropertiesRotate(animationProperty);
                break;
        }
    }

    /// <summary>
    /// Draws the full list for an animation property
    /// </summary>
    /// <param name="listIndex">Index of the list in m_parameterLists</param>
    protected virtual void DrawAnimPropertyList(int listIndex)
    {
        m_isListOpenFlags[listIndex] = EditorGUILayout.Foldout(m_isListOpenFlags[listIndex], GetTitleForList(listIndex));
        // Draw elements only if not collapsed
        if (m_isListOpenFlags[listIndex])
        {
            EditorGUI.indentLevel++;
            m_parameterLists[listIndex].DoLayoutList();
            EditorGUI.indentLevel--;
        }
    }

    /// <summary>
    /// Get the title for the animation params list.
    /// </summary>
    /// <param name="listIndex">List to get the title for.</param>
    /// <returns>If listIndex is smaller than 5, c_listTitles[listIndex]. Else, an empty string.</returns>
    protected virtual string GetTitleForList(int listIndex)
    {
        if (listIndex < c_listTitles.Length) return c_listTitles[listIndex];
        else return "";
    }

    /// <summary>
    /// Make sure the supplied property has at least requiredSize amount of elements.
    /// </summary>
    /// <param name="property"></param>
    /// <param name="requiredSize"></param>
    protected void CheckPropertyVectorSize(SerializedProperty property, int requiredSize)
    {
        while (property.arraySize < requiredSize) { property.InsertArrayElementAtIndex(property.arraySize); }
    }


    #region Draw Calls based on Types
    /// <summary>
    /// Draws animation properties for the type Custom. Can be overriden if a custom layout for specific custom animations speeds up development.
    /// </summary>
    protected virtual void DrawAnimPropertiesCustom(SerializedProperty animationProperty)
    {
        DrawAnimPropertiesAll();
    }

    /// <summary>
    /// Draws lists for all the animation parameters. Used by DrawAnimPropertiesCustom by default.
    /// </summary>
    protected virtual void DrawAnimPropertiesAll()
    {
        for (int i = 0; i < m_parameterLists.Count; i++)
        {
            DrawAnimPropertyList(i);
        }
    }

    /// <summary>
    /// Draw 3 boolean fields to activate and deactivate different axes, and 2 sets of 3 float parameters.
    /// </summary>
    /// <param name="boolsLabel"></param>
    /// <param name="array1Label"></param>
    /// <param name="array2Label"></param>
    protected void DrawPerAxisFields(string boolsLabel, string array1Label, string array2Label)
    {
        // Make sure the bool property has an element for each axis
        // Make sure the float property has enough elements to make up 2 vectors
        CheckPropertyVectorSize(m_boolParams, 3);
        CheckPropertyVectorSize(m_floatParams, 6);

        #region Toggles
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 47.0f;
        EditorGUILayout.LabelField(boolsLabel);
        EditorGUIUtility.labelWidth = 28.0f;
        m_boolParams.GetArrayElementAtIndex(0).boolValue = EditorGUILayout.Toggle("X", m_boolParams.GetArrayElementAtIndex(0).boolValue);
        m_boolParams.GetArrayElementAtIndex(1).boolValue = EditorGUILayout.Toggle("Y", m_boolParams.GetArrayElementAtIndex(1).boolValue);
        m_boolParams.GetArrayElementAtIndex(2).boolValue = EditorGUILayout.Toggle("Z", m_boolParams.GetArrayElementAtIndex(2).boolValue);
        GUILayout.EndHorizontal();
        #endregion

        #region Start Size
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 87.0f;
        EditorGUILayout.LabelField(array1Label);
        EditorGUIUtility.labelWidth = 28.0f;

        EditorGUI.BeginDisabledGroup(!m_boolParams.GetArrayElementAtIndex(0).boolValue);
        m_floatParams.GetArrayElementAtIndex(0).floatValue = EditorGUILayout.FloatField("X", m_floatParams.GetArrayElementAtIndex(0).floatValue);
        EditorGUI.EndDisabledGroup();

        EditorGUI.BeginDisabledGroup(!m_boolParams.GetArrayElementAtIndex(1).boolValue);
        m_floatParams.GetArrayElementAtIndex(1).floatValue = EditorGUILayout.FloatField("Y", m_floatParams.GetArrayElementAtIndex(1).floatValue);
        EditorGUI.EndDisabledGroup();

        EditorGUI.BeginDisabledGroup(!m_boolParams.GetArrayElementAtIndex(2).boolValue);
        m_floatParams.GetArrayElementAtIndex(2).floatValue = EditorGUILayout.FloatField("Z", m_floatParams.GetArrayElementAtIndex(2).floatValue);
        EditorGUI.EndDisabledGroup();
        GUILayout.EndHorizontal();
        #endregion

        #region End Size
        GUILayout.BeginHorizontal();
        EditorGUIUtility.labelWidth = 87.0f;
        EditorGUILayout.LabelField(array2Label);
        EditorGUIUtility.labelWidth = 28.0f;

        EditorGUI.BeginDisabledGroup(!m_boolParams.GetArrayElementAtIndex(0).boolValue);
        m_floatParams.GetArrayElementAtIndex(3).floatValue = EditorGUILayout.FloatField("X", m_floatParams.GetArrayElementAtIndex(3).floatValue);
        EditorGUI.EndDisabledGroup();

        EditorGUI.BeginDisabledGroup(!m_boolParams.GetArrayElementAtIndex(1).boolValue);
        m_floatParams.GetArrayElementAtIndex(4).floatValue = EditorGUILayout.FloatField("Y", m_floatParams.GetArrayElementAtIndex(4).floatValue);
        EditorGUI.EndDisabledGroup();

        EditorGUI.BeginDisabledGroup(!m_boolParams.GetArrayElementAtIndex(2).boolValue);
        m_floatParams.GetArrayElementAtIndex(5).floatValue = EditorGUILayout.FloatField("Z", m_floatParams.GetArrayElementAtIndex(5).floatValue);
        EditorGUI.EndDisabledGroup();
        GUILayout.EndHorizontal();
        #endregion
    }

    /// <summary>
    /// Draw properties for translate animations. By default, these are the Start Position and End Position.
    /// </summary>
    protected void DrawAnimPropertiesTranslate(SerializedProperty animationProperty)
    {
        DrawPerAxisFields("Translate Axis", "Show Translation", "Hide Translation");
    }

    /// <summary>
    /// Draw properties for scale animations. By default, these are the Start Size and End Size.
    /// </summary>
    protected void DrawAnimPropertiesScale(SerializedProperty animationProperty)
    {
        DrawPerAxisFields("Scale Axis", "Show Scale", "Hide Scale");
    }

    /// <summary>
    /// Draw properties for rotate animations. By default, these are the Start Rotation and End Rotation.
    /// </summary>
    protected void DrawAnimPropertiesRotate(SerializedProperty animationProperty)
    {
        DrawPerAxisFields("Rotate Axis", "Show Rotation", "Hide Rotation");
    }

    /// <summary>
    /// Draw properties for slide animations. By default, these are the direction of the slide, the normalized values and quick settings.
    /// </summary>
    protected virtual void DrawAnimPropertiesSlide(SerializedProperty animationProperty)
    {
        // Make sure the float property has enough elements to make up a vector.
        CheckPropertyVectorSize(m_vectorParams, 1);
        // Convert saved vector parameter to angle for display and editing.
        Vector3 directionVector = m_vectorParams.GetArrayElementAtIndex(0).vector3Value;
        float angle = Mathf.Acos(directionVector.x);
        angle *= Mathf.Rad2Deg;
        if (directionVector.y < 0) angle *= -1;
        angle = EditorGUILayout.FloatField("Slide Direction Angle", angle);
        // Convert the angle back and save it.
        directionVector = Quaternion.Euler(0, 0, angle) * Vector3.right;
        m_vectorParams.GetArrayElementAtIndex(0).vector3Value = directionVector;
    }
    #endregion
    #endregion
    #endregion
}

public class StackElementGizmoDrawer
{
    /// <summary>
    /// Gizmo to show in which direction the slide animation moves.
    /// </summary>
    /// <param name="element"></param>
    /// <param name="gizmoType"></param>
    [DrawGizmo(GizmoType.Selected | GizmoType.Active)]
    static void DrawSlideDirectionGizmo(StackElement element, GizmoType gizmoType)
    {
        /*
        if (element.transitionAnimation == StackElement.StackAnimation.Slide)
        {
            Gizmos.color = Color.gray;
            Vector3 target1 = element.transform.position + element.vectorParams[0].normalized * 100;
            Gizmos.DrawLine(element.transform.position, target1);
            Gizmos.DrawSphere(target1, 10);


            Gizmos.color = Color.red;
            Vector3 target2 = element.transform.position + -element.vectorParams[0].normalized * 100;
            Gizmos.DrawLine(element.transform.position, target2);
            Gizmos.DrawSphere(target2, 10);
        }
        */
    }
}
#endif
