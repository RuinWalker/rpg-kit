﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(DataSkill))]
public class DataSkillEditor : DataUsableItemEditor
{

    protected override void OnEnable()
    {
        base.OnEnable();
    }

    public override void OnInspectorGUI()
    {
        // Draw all base properties
        base.OnInspectorGUI();

        serializedObject.ApplyModifiedProperties();
    }


}
