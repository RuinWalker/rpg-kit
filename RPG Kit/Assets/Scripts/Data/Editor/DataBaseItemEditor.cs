﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class DataBaseItemEditor : Editor {
    // icon
    // feature list
    // display name
    // id?


    public override void OnInspectorGUI()
    {
        // Draw all base properties
        base.OnInspectorGUI();

        EditorGUILayout.LabelField(serializedObject.FindProperty("m_displayName").stringValue);

        serializedObject.ApplyModifiedProperties();
    }
}
