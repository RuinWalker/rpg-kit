﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

public class DataUsableItemEditor : DataBaseItemEditor {

    // scope
    // targets
    // success rate
    // hit type
    // damage
    // folow up items

    protected ReorderableList m_effectsList;
    protected SerializedProperty m_effectsProperty;

    protected virtual void OnEnable()
    {
        m_effectsProperty = serializedObject.FindProperty("m_effects");
        m_effectsList = CreateEffectsList();
    }

    // values should be saved while editing
    // default values per effect
    protected virtual ReorderableList CreateEffectsList()
    {
        ReorderableList list = new ReorderableList(serializedObject, m_effectsProperty, true, false, true, true);
        list.drawHeaderCallback =
        (Rect rect) =>
        {
            EditorGUI.LabelField(rect, "Effects");
        };

        list.drawElementCallback =
        (Rect rect, int index, bool isActive, bool isFocused) =>
        {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);
            rect.y += 2;
            string label = "Element " + index.ToString();
            // Draw element field type based on property type.
            DataUsableItem.Effect.EffectType effectType = (DataUsableItem.Effect.EffectType)(element.FindPropertyRelative("m_code").enumValueIndex);

            //EditorGUI.LabelField(rect, effectType.ToString());
            EditorGUI.PropertyField(
                new Rect(rect.x, rect.y, 120, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("m_code"),
                GUIContent.none
            );
            switch (effectType)
            {
                case DataUsableItem.Effect.EffectType.RecoverHP:
                case DataUsableItem.Effect.EffectType.RecoverMP:
                    element.FindPropertyRelative("m_value1").floatValue = EditorGUI.FloatField(
                        new Rect(rect.x + 120 + 8, rect.y, 60, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("m_value1").floatValue * 100
                    ) / 100;
                    EditorGUI.LabelField(
                        new Rect(rect.x + 180 + 8, rect.y, 30, EditorGUIUtility.singleLineHeight),
                        "% + "
                    );
                    element.FindPropertyRelative("m_value2").floatValue = EditorGUI.FloatField(
                        new Rect(rect.x + 210 + 12, rect.y, 60, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("m_value2").floatValue
                    );

                    break;
                case DataUsableItem.Effect.EffectType.AddState:
                case DataUsableItem.Effect.EffectType.RemoveState:
                    EditorGUI.PropertyField(
                        new Rect(rect.x + 120 + 8, rect.y, 240, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("m_valueData"),
                        GUIContent.none
                    );
                    element.FindPropertyRelative("m_value1").floatValue = EditorGUI.FloatField(
                        new Rect(rect.x + 360 + 24, rect.y, 60, EditorGUIUtility.singleLineHeight),
                        element.FindPropertyRelative("m_value1").floatValue * 100
                    ) / 100;
                    EditorGUI.LabelField(
                        new Rect(rect.x + 420 + 24, rect.y, 24, EditorGUIUtility.singleLineHeight),
                        "%"
                    );
                    break;
            }
        };
        return list;
    }

    public override void OnInspectorGUI()
    {
        // Draw all base properties
        base.OnInspectorGUI();
        m_effectsList.DoLayoutList();

        SerializedProperty usableRange = serializedObject.FindProperty("m_usableRange");
        SerializedProperty range = serializedObject.FindProperty("m_range");

        EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(EditorGUIUtility.currentViewWidth / 2));
        EditorGUILayout.LabelField("Usable range");
        EditorGUILayout.LabelField("Targettable range");
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(200));
        // reverse order as party is looking the other way
        usableRange.GetArrayElementAtIndex(3).boolValue = EditorGUILayout.Toggle(usableRange.GetArrayElementAtIndex(3).boolValue);
        usableRange.GetArrayElementAtIndex(2).boolValue = EditorGUILayout.Toggle(usableRange.GetArrayElementAtIndex(2).boolValue);
        usableRange.GetArrayElementAtIndex(1).boolValue = EditorGUILayout.Toggle(usableRange.GetArrayElementAtIndex(1).boolValue);
        usableRange.GetArrayElementAtIndex(0).boolValue = EditorGUILayout.Toggle(usableRange.GetArrayElementAtIndex(0).boolValue);
        EditorGUILayout.EndHorizontal();
        GUILayout.Space(20);
        EditorGUILayout.BeginHorizontal(GUILayout.MaxWidth(200));
        range.GetArrayElementAtIndex(0).boolValue = EditorGUILayout.Toggle(range.GetArrayElementAtIndex(0).boolValue);
        range.GetArrayElementAtIndex(1).boolValue = EditorGUILayout.Toggle(range.GetArrayElementAtIndex(1).boolValue);
        range.GetArrayElementAtIndex(2).boolValue = EditorGUILayout.Toggle(range.GetArrayElementAtIndex(2).boolValue);
        range.GetArrayElementAtIndex(3).boolValue = EditorGUILayout.Toggle(range.GetArrayElementAtIndex(3).boolValue);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
