﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class DataClassParameterGenerateWindow : EditorWindow {

    private void OnEnable()
    {
        titleContent.text = "Generate ";   
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();
        {
            EditorGUIUtility.labelWidth = 50.0f;
            EditorGUILayout.IntField("Level 1:", 0);
            EditorGUILayout.IntField("Max Level: ", 0);
        }
        GUILayout.EndHorizontal();

        // growth slider

        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("OK"))
            {
                // apply stats
                Close();
            }
            if (GUILayout.Button("Cancel")) { Close(); }
        }
        GUILayout.EndHorizontal();
    }
}
#endif
