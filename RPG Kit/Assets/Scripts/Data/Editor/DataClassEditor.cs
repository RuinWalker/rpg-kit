﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(DataClass))]
public class DataClassEditor : Editor {

    private int m_selectedParamTab;

    private int m_oldMaxLevel;

    SerializedProperty m_maxLevel;
    SerializedProperty m_parameters;

    private void OnEnable()
    {
        //
        m_maxLevel = serializedObject.FindProperty("m_maxLevel");
        m_parameters = serializedObject.FindProperty("m_parameters");

        FillParametersArray();
        /*
        for(int i = 0; i < 8; i++) // 8 should be stats count
        {
            for (int j = m_parameters.GetArrayElementAtIndex(i).arraySize; j < m_maxLevel.intValue; j++)
            {
                m_parameters.GetArrayElementAtIndex(i).InsertArrayElementAtIndex(j);
            }


            if (i == m_parameters.arraySize) { m_parameters.InsertArrayElementAtIndex(i); }
            for (int j = m_parameters.GetArrayElementAtIndex(i).arraySize; j < m_maxLevel.intValue; j++)
            {
                m_parameters.GetArrayElementAtIndex(i).InsertArrayElementAtIndex(j);
            }
        }
        */

    }

    private void FillParametersArray()
    {
        for (int i = m_parameters.arraySize; i < 8 * (m_maxLevel.intValue + 1); i++)
        {
            m_parameters.InsertArrayElementAtIndex(i);
            m_parameters.GetArrayElementAtIndex(i).intValue = 0;
        }
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (m_maxLevel.intValue != m_oldMaxLevel)
        {
            FillParametersArray();
            m_oldMaxLevel = m_maxLevel.intValue;
        }

        m_selectedParamTab = GUILayout.Toolbar(m_selectedParamTab, new string[] { "Max HP", "Max MP", "Attack", "Defence" });
        m_selectedParamTab = 4 + GUILayout.Toolbar(m_selectedParamTab - 4, new string[] { "Magic", "Magic Def.", "Agility", "Luck" });

       // EditorGUILayout.PropertyField(m_parameters.GetArrayElementAtIndex(m_selectedParamTab));

        GUILayout.BeginHorizontal();
        {
            EditorGUIUtility.labelWidth = 50.0f;
            EditorGUILayout.IntField("Level 1:", 0);
            EditorGUILayout.IntField("Max Level: ", 0);
        }
        GUILayout.EndHorizontal();

        // growth slider

        GUILayout.BeginHorizontal();
        {
            if (GUILayout.Button("Generate"))
            {
                /*
                DataClassParameterGenerateWindow window = CreateInstance<DataClassParameterGenerateWindow>();
                window.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 75);
                window.Show();
                */

                // set stats
            }
        }
        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
#endif
