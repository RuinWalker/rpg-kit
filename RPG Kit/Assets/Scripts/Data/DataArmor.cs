﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataArmor : DataEquipItem {
    public enum ArmorType { };

    public ArmorType armorType { get { return m_armorType; } }

    [SerializeField] private ArmorType m_armorType;

    public override bool isArmor { get { return true; } }
}
