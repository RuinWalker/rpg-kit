﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Actor", menuName = "Data/Actor")]
public class DataActor : DataClass {

    public int classId { get { return m_classId; } }
    public int initialLevel { get { return m_initialLevel; } }
    public Sprite battlerSprite { get { return m_battlerSprite; } }
    public DataEquipItem[] equips { get { return m_equips; } }
    public bool useActorStats { get { return m_useActorStats; } }
    public bool useActorExp { get { return m_useActorExp; } }

    [SerializeField] private int m_classId;
    [SerializeField] private int m_initialLevel;
    [SerializeField] private Sprite m_battlerSprite;
    [SerializeField] private DataEquipItem[] m_equips;
    [SerializeField] private bool m_useActorStats;
    [SerializeField] private bool m_useActorExp;

    public override bool isActor { get { return true; } }
}
