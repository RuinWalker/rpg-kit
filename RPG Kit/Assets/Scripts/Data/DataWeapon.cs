﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataWeapon : DataEquipItem {
    public enum WeaponType { none };
    public enum WearStyle { oneHanded, twoHanded, mainHanded };

    public WeaponType weaponType { get { return m_weaponType; } }
    public DataUsableItem attackSkill { get { return m_attackSkill; } }
    public WearStyle wearStyle { get { return m_wearStyle; } }

    //TODO: animation

    [SerializeField] private WeaponType m_weaponType;
    [SerializeField] private DataUsableItem m_attackSkill;
    [SerializeField] private WearStyle m_wearStyle;

    public override bool isWeapon { get { return true; } }
}
