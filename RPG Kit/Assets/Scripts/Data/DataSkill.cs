﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Skill", menuName = "Data/Skill")]
public class DataSkill : DataUsableItem {
    public enum SkillType { }

    [System.Serializable]
    public struct StateCost
    {
        public DataState state { get { return m_state; } }
        public int stacks { get { return m_stacks; } }
        [SerializeField] private DataState m_state;
        [SerializeField] private int m_stacks;
    }

    public SkillType skillType { get { return m_skillType; } }
    public List<StateCost> statesCost { get { return m_statesCost; } }
    public int mpCost { get { return m_mpCost; } }
    public int tpCost { get { return m_tpCost; } }
    public List<DataWeapon.WeaponType> requiredWeaponTypes { get { return m_requiredWeaponTypes; } }

    [SerializeField] private SkillType m_skillType;
    [SerializeField] private int m_mpCost;
    [SerializeField] private int m_tpCost;
    [SerializeField] private List<StateCost> m_statesCost;
    [SerializeField] private List<DataWeapon.WeaponType> m_requiredWeaponTypes;

    public override bool isSkill { get { return true; } }
}
