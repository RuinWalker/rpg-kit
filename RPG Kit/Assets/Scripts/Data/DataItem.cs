﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Item", menuName = "Data/Item")]
public class DataItem : DataUsableItem
{
    public enum ItemType { regular, keyItem };

    public ItemType itemType { get { return m_itemType; } }
    public int price { get { return m_price; } }
    public bool consumable { get { return m_consumable; } }

    [SerializeField] private ItemType m_itemType;
    [SerializeField] private int m_price;
    [SerializeField] private bool m_consumable;

    public bool isKeyItem { get { return m_itemType == ItemType.keyItem; } }

    public override bool isItem { get { return true; } }
}
