﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataEquipItem : DataBaseItem {
    public enum EquipType { weapon, offHand, body, head, accessory };

    public static readonly EquipType[] slots = { EquipType.weapon, EquipType.offHand, EquipType.body, EquipType.head, EquipType.accessory }; // tODO: move to settings scriptable object

    public int price { get { return m_price; } }
    public EquipType equipType { get { return m_equipType; } }
    public int[] parameters { get { return m_parameters; } }

    [SerializeField] private int m_price;
    [SerializeField] private EquipType m_equipType;
    [SerializeField] private int[] m_parameters;

    public override bool isEquip { get { return true; } }
}
