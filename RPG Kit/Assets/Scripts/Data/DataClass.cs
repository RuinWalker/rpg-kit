﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Class", menuName = "Data/Class")]
public class DataClass : DataBaseItem {

    [System.Serializable]
    public class Learning
    {
        public int level { get { return m_level; } }
        public DataSkill skill { get { return m_skill; } }
        public string note { get { return m_note; } }

        [SerializeField] private int m_level;
        [SerializeField] private DataSkill m_skill;
        [SerializeField] private string m_note;
    }

    public int maxLevel { get { return m_maxLevel; } }
    public int[] parameters { get { return m_parameters; } }
    public int[] expParameters { get { return m_expParameters; } }
    public Learning[] learnings { get { return m_learnings; } }

    [SerializeField] private int m_maxLevel;
    [SerializeField] private int[] m_parameters;
    [SerializeField] private int[] m_expParameters;
    [SerializeField] private Learning[] m_learnings;

    public int ExpForLevel(int a_level)
    {
        return Mathf.RoundToInt(m_expParameters[0] *
            Mathf.Pow(a_level - 1, 0.9f + m_expParameters[2] / 250) *
            a_level *
            (a_level + 1) /
            (6 + Mathf.Pow(a_level, 2) / 50 / m_expParameters[3]) +
            (a_level - 1) *
            m_expParameters[1]);
    }

    public override bool isClass { get { return true; } }

}
