﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New State", menuName = "Data/State")]
public class DataState : DataBaseItem
{
    public enum Restriction { none, attackEnemy, attackAlly, attackEnemyOrAlly, unableToAct }

    public int maxStacks { get { return m_maxStacks; } }
    public bool featuresOnlyOnce { get { return m_featuresOnlyOnce; } }
    public Restriction restriction { get { return m_restriction; } }
    public int priority { get { return m_priority; } }
    public bool removeAtBattleEnd { get { return m_removeAtBattleEnd; } }
    public bool removeByRestriction { get { return m_removeByRestriction; } }
    public bool removePerStack { get { return m_removePerStack; } }
    public int autoRemovalTiming { get { return m_autoRemovalTiming; } }
    public int minTurns { get { return m_minTurns; } }
    public int maxTurns { get { return m_maxTurns; } }
    public bool removeByDamage { get { return m_removeByDamage; } }
    public int chanceByDamage { get { return m_chanceByDamage; } }
    /// <summary>
    /// Minimum damage for removing the state by damage.
    /// </summary>
    public int minByDamage { get { return m_minByDamage; } }
    /// <summary>
    /// Maximum damage for removing the state by damage.
    /// </summary>
    public int maxByDamage { get { return m_maxByDamage; } }

    [SerializeField] private int m_maxStacks;
    [SerializeField] private bool m_featuresOnlyOnce;
    [SerializeField] private Restriction m_restriction;
    [SerializeField] private int m_priority;
    [SerializeField] private bool m_removeAtBattleEnd;
    [SerializeField] private bool m_removeByRestriction;
    [SerializeField] private bool m_removePerStack;
    [SerializeField] private int m_autoRemovalTiming;
    [SerializeField] private int m_minTurns;
    [SerializeField] private int m_maxTurns;
    [SerializeField] private bool m_removeByDamage;
    [SerializeField] private int m_chanceByDamage;
    [SerializeField] private int m_minByDamage;
    [SerializeField] private int m_maxByDamage;

    public override bool isState { get { return true; } }
}
