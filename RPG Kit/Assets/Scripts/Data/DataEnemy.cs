﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "Data/Enemy")]
public class DataEnemy : DataBaseItem {

    [System.Serializable]
    public class DropItem
    {
        public DataBaseItem item { get { return m_item; } }
        public float dropChance { get { return m_dropChance; } }

        [SerializeField] private DataBaseItem m_item;
        [SerializeField] private float m_dropChance;
    }

    [System.Serializable]
    public class Action
    {
        public enum Condition { always, turnNumber, hp, mp, state, partyLevel, flag };

        public DataUsableItem item { get { return m_item; } }
        public Condition conditionType { get { return m_conditionType; } }
        public float conditionValue1 { get { return m_conditionValue1; } }
        public float conditionValue2 { get { return m_conditionValue2; } }
        public int rating { get { return m_rating; } }

        [SerializeField] DataUsableItem m_item;
        [SerializeField] Condition m_conditionType;
        [SerializeField] float m_conditionValue1;
        [SerializeField] float m_conditionValue2;
        [SerializeField] int m_rating;
    }

    public Sprite battlerSprite { get { return m_battlerSprite; } }
    public int[] parameters {get {return m_parameters; }}
    public int exp { get { return m_exp; } }
    public int gold { get { return m_gold; } }
    public DropItem[] dropItems { get { return m_dropItems; } }
    public Action[] actions { get { return m_actions; } }

    [SerializeField] private Sprite m_battlerSprite;
    [SerializeField] private int[] m_parameters;
    [SerializeField] private int m_exp;
    [SerializeField] private int m_gold;
    [SerializeField] private DropItem[] m_dropItems;
    [SerializeField] private Action[] m_actions;

}
