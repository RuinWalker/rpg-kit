﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataTroop : ScriptableObject {

    [System.Serializable]
	public class Member
    {
        [SerializeField] private DataEnemy m_enemy;
        // position
        // hidden
    }

    [System.Serializable]
    public class Page
    {
        // events
    }

    public int id { get { return m_id; } }
    public Member[] members { get { return m_members; } }

    private int m_id;
    [SerializeField] private Member[] m_members;

    public void SetId(int a_id) { m_id = a_id; } // function so that id is not mistaken to be used for assigning. Should only be used in DataManager.
}
