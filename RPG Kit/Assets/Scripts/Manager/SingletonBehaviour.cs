﻿using UnityEngine;

public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// Singleton-Reference
    /// </summary>
    public static T Instance
    {
        get
        {
            if (m_instance == null)
            {
                GameObject obj = new GameObject();
                obj.name = "Instantiated Manager";
                obj.AddComponent<T>();
            }
            return m_instance;
        }
        protected set { m_instance = value; }
    }

    static private T m_instance;

    #endregion

    #region Methods
    /// <summary>
    /// Singleton-Setup
    /// </summary>
    public virtual void Awake()
    {
        if (m_instance != null && m_instance != this)
        {
            Debug.LogError("Singleton<" + typeof(T) + "> already exists! Destroying object " + gameObject.name);
            Destroy(gameObject);
        }
        m_instance = this as T;
    }

    /// <summary>
    /// Singleton-Destruction
    /// </summary>
    public virtual void OnDestroy()
    {
        if (m_instance.Equals(this))
            m_instance = null;
    }
    #endregion
}