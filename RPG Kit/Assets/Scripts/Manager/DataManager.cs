﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

static public class DataManager
{

    static public List<DataClass> classes { get { return m_classes; } }
    static public List<DataActor> actors { get { return m_actors; } }
    static public List<DataEnemy> enemies { get { return m_enemies; } }
    static public List<DataSkill> skills { get { return m_skills; } }
    static public List<DataItem> items { get { return m_items; } }
    static public List<DataBaseItem> weapons { get { return m_weapons; } }
    static public List<DataBaseItem> armors { get { return m_armors; } }
    static public List<DataState> states { get { return m_states; } }

    static DataManager()
    {
        m_classes = new List<DataClass>(Resources.LoadAll<DataClass>(c_dataClassesPath));
        for (int i = 0; i < m_classes.Count; i++) { m_classes[i].SetId(i); }
        m_actors = new List<DataActor>(Resources.LoadAll<DataActor>(c_dataActorsPath));
        for (int i = 0; i < m_actors.Count; i++) { m_actors[i].SetId(i); }
        m_enemies = new List<DataEnemy>(Resources.LoadAll<DataEnemy>(c_dataEnemiesPath));
        for (int i = 0; i < m_enemies.Count; i++) { m_enemies[i].SetId(i); }
        m_skills = new List<DataSkill>(Resources.LoadAll<DataSkill>(c_dataSkillsPath));
        for (int i = 0; i < m_skills.Count; i++) { m_skills[i].SetId(i); }
        m_items = new List<DataItem>(Resources.LoadAll<DataItem>(c_dataItemsPath));
        for (int i = 0; i < m_items.Count; i++) { m_items[i].SetId(i); }
        m_weapons = new List<DataBaseItem>(Resources.LoadAll<DataBaseItem>(c_dataWeaponsPath));
        for (int i = 0; i < m_weapons.Count; i++) { m_weapons[i].SetId(i); }
        m_armors = new List<DataBaseItem>(Resources.LoadAll<DataBaseItem>(c_dataArmorsPath));
        for (int i = 0; i < m_armors.Count; i++) { m_armors[i].SetId(i); }
        m_states = new List<DataState>(Resources.LoadAll<DataState>(c_dataStatesPath));
        for (int i = 0; i < m_states.Count; i++) { m_states[i].SetId(i); }
    }

    static private void SetIds(List<DataBaseItem> m_list)
    {
        for (int i = 0; i < m_list.Count; i++) { m_list[i].SetId(i); }
    }

    static private List<DataClass> m_classes;
    static private List<DataActor> m_actors;
    static private List<DataEnemy> m_enemies;
    static private List<DataSkill> m_skills;
    static private List<DataItem> m_items;
    static private List<DataBaseItem> m_weapons;
    static private List<DataBaseItem> m_armors;
    static private List<DataState> m_states;

    const string c_dataPath = "RPG Data/";
    const string c_dataClassesPath = c_dataPath + "Classes";
    const string c_dataActorsPath = c_dataPath + "Actors";
    const string c_dataEnemiesPath = c_dataPath + "Enemies";
    const string c_dataSkillsPath = c_dataPath + "Skills";
    const string c_dataItemsPath = c_dataPath + "Items";
    const string c_dataWeaponsPath = c_dataPath + "Weapons";
    const string c_dataArmorsPath = c_dataPath + "Armors";
    const string c_dataStatesPath = c_dataPath + "States";

    static public int GetIndex(DataClass a_class) { return classes.IndexOf(a_class); }
    static public int GetIndex(DataActor a_actor) { return actors.IndexOf(a_actor); }
    static public int GetIndex(DataEnemy a_enemy) { return enemies.IndexOf(a_enemy); }
    static public int GetIndex(DataSkill a_skill) { return skills.IndexOf(a_skill); }
    static public int GetIndex(DataItem a_item) { return items.IndexOf(a_item); }
    static public int GetIndex(DataBaseItem a_weapon) { return weapons.IndexOf(a_weapon); }
    static public int GetIndex(DataState a_state) { return states.IndexOf(a_state); }
    
}
