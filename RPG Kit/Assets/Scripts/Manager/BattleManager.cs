﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

/*
MODE NOTES
    Party command selection & Actor Command selection
    Next command goes to next inputtable actor
    Allow for prior command

    Start turn...
    On next character, next character in turn order should input/act

    Start ATB
    On character finish charge, should input/act

    Start Input function, ends either in next command or start action
    Party actor command selection, allows next command and ends in end command
    Start action function, does the actual action stuff
    Give action function, start of individual character's action section in turn order battle systems
    */

public class BattleManager : SingletonBehaviour<BattleManager> {

    public enum Phase { none, input, turnStart, turn, turnEnd, aborting }
    public enum BattleResult { victory, defeat, abort }
    public delegate void VoidDelegate();

    #region Static

    static private bool m_battleTest = true;

    static public void OnEncounter()
    {
        // determine surprise/preemptive
    }

    static public float rateSurprise { get { return 0f; } }
    static public float ratePreemptive { get { return 0f; } }

    // save bgm and bgs

    // play battle end me

    // resume bgm and bgs

    // make escape ratio

    static public bool CurrentPhaseEquals(Phase a_phase) { return Instance.m_currentPhase == a_phase; }
    static public bool isInTurn { get { return CurrentPhaseEquals(Phase.turn); } }
    static public bool isTurnEnd { get { return CurrentPhaseEquals(Phase.turnEnd); } }
    static public bool isAborting { get { return CurrentPhaseEquals(Phase.aborting); } }

    // can escape

    // event proc

    // method wait for message

    // wait for message

    // display exp

    // display gold

    // gain exp

    // gain gold

    // gain items
    #endregion

    #region TEMP
    public CanvasBattle canvas;
    #endregion

    /*
    Modes:

    Stages:


    Modular: stuff
    Turn order
    When input takes place
    Turn marker
    Action marker

    */

    #region Variables
    #region Public
    /// <summary>
    /// The unit who currently has its turn
    /// </summary>
    public GameBattler subject { get; private set; }

    // TODO: NEEDS ERROR HANDLING
    public GameActor actor { get { return GameParty.instance.battleMembers[m_actorIndex] as GameActor; } }

    // TODO: should be cached
    public List<GameBattler> allBattleMembers
    {
        get
        {
            List<GameBattler> result = new List<GameBattler>(GameParty.instance.battleMembers.Count + GameTroop.instance.members.Count);
            result.AddRange(GameParty.instance.battleMembers);
            result.AddRange(GameTroop.instance.members);
            return result;
        }
    }
    #endregion

    #region Private

    #endregion
    
    /// <summary>
    /// Entry point at the start of a turn
    /// </summary>
    private VoidDelegate turnEntryDelegate;

    /// <summary>
    /// Exit point on input finish
    /// </summary>
    private VoidDelegate inputFinishDelegate;

    /// <summary>
    /// Entroy point at the start of an action
    /// </summary>
    private VoidDelegate actionEntryDelegate;

    /// <summary>
    /// Current phase of the battle system
    /// </summary>
    private Phase m_currentPhase;

    /// <summary>
    /// Index of the current inputtable actor
    /// </summary>
    private int m_actorIndex;

    /// <summary>
    /// Action order for all units
    /// </summary>
    private List<GameBattler> m_battlersActionOrder;

    /// <summary>
    /// The time till which the battle system has to wait
    /// </summary>
    private float m_waitFlag;
    #endregion

    #region Methods
    #region Unity
    private void Start()
    {
        InitMembers();

        Setup(); // Temporary for testing purposes
    }

    // check if has to wait for log
    private void Update()
    {
        if (Time.time >= m_waitFlag && !canvas.isBusy)
        {
            if (isInTurn)
            {
                // process event
                //ProcessAction();
            }
            //JudgeWinLoss();
        }
    }
    #endregion

    #region Public
    static public void Setup()
    {
        GameTroop.instance.Setup();
        Instance.SetupUI();
        Instance.BattleStart();
    }

    static public void Abort()
    {
        Instance.m_currentPhase = Phase.aborting;
    }

    static public bool JudgeWinLoss()
    {
        BattleManager instance = Instance;
        if (instance.m_currentPhase != Phase.none)
        {
            if (GameParty.instance.members.Count == 0) { return instance.ProcessAbort(); }
            if (GameParty.instance.areAllDead) { return instance.ProcessDefeat(); }
            if (GameTroop.instance.areAllDead) { return instance.ProcessVictory(); }
        }
        return false;
    }

    public void Wait(float a_time)
    {
        m_waitFlag = Time.time + a_time;
    }
    #endregion

    #region Private
    private void InitMembers()
    {
        m_currentPhase = Phase.turnStart;
        m_battlersActionOrder = new List<GameBattler>();
        subject = null;
        ClearActor();
        InitializePartyOrderSettings();
        InitializeTurnOrderSettings();
    }

    [Conditional("PARTY_ORDER")]
    private void InitializePartyOrderSettings()
    {
        turnEntryDelegate = new VoidDelegate(InputStart);
        inputFinishDelegate = new VoidDelegate(NextCommand);
        actionEntryDelegate = new VoidDelegate(ActionStart);
    }

    [Conditional("TURN_ORDER")]
    private void InitializeTurnOrderSettings()
    {
        turnEntryDelegate = new VoidDelegate(TurnStart);
        inputFinishDelegate = new VoidDelegate(ActionStart);
        actionEntryDelegate = new VoidDelegate(PreActionStart);
    }

    private void SetupUI()
    {
        canvas.Setup();
    }

    /// <summary>
    /// Starts the battle loop.
    /// </summary>
    private void BattleStart()
    {
        /*
        options:
        Party command selection
        Actor command selection
        Start turn, make order and give first unit the turn right
        Start ATB
        */
        UnityEngine.Debug.Log("Battle starts");
        GameParty.instance.OnBattleStart();
        GameTroop.instance.OnBattleStart();
        turnEntryDelegate.Invoke();
    }

    private void InputStart()
    {
        m_currentPhase = Phase.input;
        GameParty.instance.MakeActions();
        GameTroop.instance.MakeActions();
        StartPartyCommandSelection(); // TODO: UNLESS !INPUTTABLE
    }

    private void TurnStart()
    {
        // close windows
        m_currentPhase = Phase.turnStart;
        ClearActor();
        // increase turn count
        MakeActionOrder();
        canvas.OnTurnStart();
        // do logs
        StartNextInOrder();
    }

    private void PreActionStart(GameBattler a_subject)
    {
        subject = a_subject;
        PreActionStart();
    }

    private void PreActionStart()
    {
        m_currentPhase = Phase.input;
        subject.MakeActions();
        if (subject.isActor)
        {
            m_actorIndex = subject.index;
            StartActorCommandSelection();
        }
        else
        {
            // determine action
            ActionStart();
        }
    }

    /// <summary>
    /// Execute subject's current action
    /// </summary>
    private void ActionStart()
    {
        if (subject.currentAction == null)
        {
            subject.OnActionStart();
            if (subject.currentAction.isValid)
            {
                canvas.OnActionStart();
                StartCoroutine(ExecuteAction());
            }
            else
                StartNextInOrder();
        }
        else
            StartNextInOrder();
    }

    /// <summary>
    /// Handles the end of the action
    /// </summary>
    private void ActionEnd()
    {
        subject.OnActionEnd();
        canvas.OnActionEnd();
    }

    /// <summary>
    /// Handles the end of the turn
    /// </summary>
    private void TurnEnd()
    {
        m_currentPhase = Phase.turnEnd;
        // preemptive and surprise
        foreach (GameBattler battler in allBattleMembers)
        {
            battler.OnTurnEnd();
            // update status
            // log
        }
        canvas.OnTurnEnd();
        // process event
        turnEntryDelegate.Invoke();
    }

    /// <summary>
    /// Handles the end of the battle
    /// </summary>
    /// <param name="a_result"></param>
    private void BattleEnd(int a_result)
    {
        // TODO: a_result should be an enum
        m_currentPhase = Phase.none;
        // proc event
        GameParty.instance.OnBattleEnd();
        GameTroop.instance.OnBattleEnd();
        if (m_battleTest) { Application.Quit(); }
        ;
    }

    private bool ProcessVictory()
    {
        // play battle end me
        // resume bgm
        // victory message
        // exp
        // gold
        // drop items
        // go to menu scene
        BattleEnd(0);
        return true;
    }

    // process escape

    private bool ProcessAbort()
    {
        // replay bgm
        // go map back
        BattleEnd(1);
        return true;
    }

    private bool ProcessDefeat()
    {
        // defeat message
        // wait for message
        // if can lose
        //  revive all
        //  replay bgm
        //  go to map
        // else
        //  go to gameover
        BattleEnd(2);
        return true;
    }

    private void MakeActionOrder()
    {
        m_battlersActionOrder.Clear();
        m_battlersActionOrder.AddRange(allBattleMembers);
        //foreach(GameBattler battler in m_actionBattlers) { battler.MakeSpeed(); }
        //TODO: action battlers sort by speed
    }

    private GameBattler NextSubject()
    {
        while (m_battlersActionOrder.Count > 0)
        {
            GameBattler battler = m_battlersActionOrder[0];
            m_battlersActionOrder.RemoveAt(0);
            if (battler != null && battler.index >= 0 && battler.isAlive)
                return battler;
        }
        return null;
    }

    private void StartNextInOrder()
    {
        subject = NextSubject();
        if (subject == null)
            TurnEnd();
        else
            actionEntryDelegate.Invoke();
    }

    /// <summary>
    /// Executes the action, applying items and removing it from the user's current actions list.
    /// </summary>
    /// <returns></returns>
    public IEnumerator ExecuteAction()
    {
        yield return UseItem(subject.currentAction, null);
        subject.RemoveCurrentAction();
        ActionStart();
        ActionEnd();
        if (!JudgeWinLoss())
            ActionStart();
    }

    public IEnumerator UseCurrentAction()
    {
        yield return UseItem(subject.currentAction, null);
        JudgeWinLoss();
    }

    /// <summary>
    /// Uses the item in the supplied action on all targets.
    /// </summary>
    /// <param name="a_action"></param>
    /// <param name="a_rootAction"></param>
    /// <returns></returns>
    public IEnumerator UseItem(GameAction a_action, GameAction a_rootAction)
    {
        DataUsableItem item = a_action.item;
        // log window display use item
        subject.UseItem(item);
        // refresh status / display effects from using item

        List<GameBattler> targets;
        if (a_rootAction == null)
            targets = a_action.MakeTargets();
        else
            targets = a_action.MakeFollowUpTargets(a_rootAction, a_rootAction.targets);
        for (int i = 0; i < item.repeats; i++)
        {
            // show animation if all
            // wait for animation
            foreach (GameBattler target in targets)
                canvas.PlayAnimation(target);
            yield return WaitForAnimations();
            foreach (GameBattler target in targets)
            {
                InvokeItem(target, item);
                ApplyOnHitItems(target, a_action);
            }
            yield return WaitForCanvas();
        }
        yield return UseFollowUpItems(a_action);
    }

    public IEnumerator UseFollowUpItems(GameAction a_rootAction)
    {
        foreach (DataUsableItem subItem in a_rootAction.item.followUpItems)
        {
            GameAction action = new GameAction(a_rootAction.subject, subItem);
            yield return UseItem(action, a_rootAction);
        }
    }

    public void InvokeItem(GameBattler a_target, DataUsableItem a_item)
    {
        // check for counter and magic reflection
        ApplyItemEffects(a_target, a_item); // check for substitute
        // set last target
    }

    public void ApplyItemEffects(GameBattler a_target, DataUsableItem a_item)
    {
        a_target.ItemApply(subject, a_item);
        // refresh status
        canvas.DisplayResults(a_target.actionResult);
        // wait for action results?
    }

    public void ApplyOnHitItems(GameBattler a_target, GameAction a_rootAction)
    {
        if (a_rootAction.item.damage.isNone)
            return; // check if was an actual hit
        foreach (DataUsableItem item in a_rootAction.subject.onHitItems)
        {
            GameAction action = new GameAction(a_rootAction.subject, item);
            List<GameBattler> targets = action.MakeOnHitTargets(a_rootAction, a_target);
            foreach (GameBattler target in targets)
            {
                InvokeItem(target, item);
            }
        }
    }

    private void StartPartyCommandSelection()
    {
        canvas.StartPartyCommandSelection();
    }

    private void StartActorCommandSelection()
    {
        canvas.StartActorCommandSelection();

        // on finish, if party
        //  call next command
        // else
        //  call start action
    }

    public void FinishActorCommandSelection()
    {
        inputFinishDelegate.Invoke();
    }

    /// <summary>
    /// Go to the next actor command if possible
    /// </summary>
    private void NextCommand()
    {
        if (NextActor())
            StartActorCommandSelection();
        else
            TurnStart();
    }

    /// <summary>
    /// Go to the previous actor command if possible
    /// </summary>
    private void PriorCommand()
    {
        if (PriorActor())
            StartActorCommandSelection();
        else
            StartPartyCommandSelection();
    }

    /// <summary>
    /// Resets the actor index
    /// </summary>
    private void ClearActor() { m_actorIndex = -1; }

    /// <summary>
    /// Get the next inputtable actor index if possible
    /// </summary>
    /// <returns>Could get a valid actor?</returns>
    private bool NextActor()
    {
        while (m_actorIndex < GameParty.instance.battleMembers.Count)
        {
            m_actorIndex++;
            if (GameParty.instance.battleMembers[m_actorIndex].isInputable)
                return true;
        }
        return false;
    }

    /// <summary>
    /// Get the prior inputtable actor index if possible
    /// </summary>
    /// <returns>Could get a valid actor?</returns>
    private bool PriorActor()
    {
        while (m_actorIndex > -1)
        {
            m_actorIndex--;
            if (GameParty.instance.battleMembers[m_actorIndex].isInputable)
                return true;
        }
        return false;
    }

    private IEnumerator WaitForCanvas()
    {
        while (canvas.isBusy) { yield return null; }
    }

    private IEnumerator WaitForPopups()
    {
        while (canvas.hasPopups) { yield return null; }
    }

    private IEnumerator WaitForAnimations()
    {
        while (canvas.hasAnimations) { yield return null; }
    }
    #endregion
    #endregion

    /*
    public void CommandAttack()
    {
        OnSkillOk(0);
    }

    public void CommandSkill()
    {
        canvas.SetSkillWindowActor(m_subject);
        // skill window show and activate
    }
    */

    /*
    /// <summary>
    /// Find the next action that has to be executed and execute it.
    /// </summary>
    public void ProcessAction()
    {
        // If no subject, or subject has no actions, go to next subject
        if (m_subject == null || m_subject.currentAction == null)
        {
            m_subject = NextSubject();
            if (c_actInOrder)
            {
                if (InputStart())
                {
                    StartActorCommandSelection();
                    return;
                }
            }
        }
        // If all subjects have acted, end turn
        if (m_subject == null) { TurnEnd(); return; }

        // If subject has action, execute action
        if (m_subject.currentAction != null)
        {
            m_subject.currentAction.Prepare();
            if (m_subject.currentAction.isValid)
            {
                canvas.OnActionStart();
                StartCoroutine(ExecuteAction());
            }
            
        }
    }

    /// <summary>
    /// Handles the action's end
    /// </summary>
    public void ProcessActionEnd()
    {
        m_subject.OnActionEnd();
        canvas.OnActionEnd();
        if (!JudgeWinLoss())
        {
            ProcessAction();
        }
    }

    /// <summary>
    /// Executes the action, applying items and removing it from the user's current actions list.
    /// </summary>
    /// <returns></returns>
    public IEnumerator ExecuteAction()
    {
        yield return UseItem(m_subject.currentAction, null);
        while (canvas.isBusy) { yield return null; }
        WaitForCanvas();
        m_subject.RemoveCurrentAction();
        if (m_subject.currentAction == null) { ProcessActionEnd(); }
    }

    /// <summary>
    /// Uses the item in the supplied action on all targets.
    /// </summary>
    /// <param name="a_action"></param>
    /// <param name="a_rootAction"></param>
    /// <returns></returns>
    public IEnumerator UseItem(GameAction a_action, GameAction a_rootAction)
    {
        DataUsableItem item = a_action.item;
        // log window display use item
        m_subject.UseItem(item);
        // refresh status / display effects from using item

        List<GameBattler> targets;
        if (a_rootAction == null) { targets = a_action.MakeTargets(); }
        else { targets = a_action.MakeFollowUpTargets(a_rootAction, a_rootAction.targets); }
        for (int i = 0; i < item.repeats; i++)
        {
            Debug.Log("Performing repeat");
            // show animation if all
            // wait for animation
            foreach (GameBattler target in targets)
            {
                canvas.PlayAnimation(target);
            }
            yield return WaitForAnimations();
            foreach (GameBattler target in targets)
            {

                InvokeItem(target, item);
                ApplyOnHitItems(target, a_action);
            }
            yield return WaitForCanvas();
        }
        yield return UseFollowUpItems(a_action);
        JudgeWinLoss();
    }

    private IEnumerator WaitForCanvas()
    {
        while (canvas.isBusy) { yield return null; }
    }

    private IEnumerator WaitForPopups()
    {
        while (canvas.hasPopups) { yield return null; }
    }

    private IEnumerator WaitForAnimations()
    {
        while (canvas.hasAnimations) { yield return null; }
    }

    public IEnumerator UseItemOld()
    {

        yield return UseItem(m_subject.currentAction, null);
        /*
        DataUsableItem item = m_subject.currentAction.item;
        // log window display use item
        m_subject.UseItem(item);
        // refresh status / display effects from using item
        List<GameBattler> targets = m_subject.currentAction.MakeTargets();
        for (int i = 0; i < item.repeats; i++)
        {
            Debug.Log("Performing repeat");
            // show animation if all
            // wait for animation
            foreach (GameBattler target in targets)
            {
                canvas.PlayAnimation(target);
                yield return WaitForAnimations();
                InvokeItem(target, item);
                ApplyOnHitItems(target, m_subject.currentAction);
            }
            yield return WaitForCanvas();
        }
        UseFollowUpItems(m_subject.currentAction);
        JudgeWinLoss();
        /
    }

    public IEnumerator UseFollowUpItems(GameAction a_rootAction)
    {
        Debug.Log("Using follow up items");
        foreach (DataUsableItem subItem in a_rootAction.item.followUpItems)
        {
            GameAction action = new GameAction(a_rootAction.subject, subItem);
            yield return UseItem(action, a_rootAction);
        }
            /*
            foreach (DataUsableItem subItem in a_rootAction.item.followUpItems)
            {
                GameAction action = new GameAction(a_rootAction.subject, subItem);
                List<GameBattler> subTargets = action.MakeFollowUpTargets(a_rootAction, a_rootAction.targets);

                // show animation
                foreach (GameBattler target in subTargets)
                {
                    for (int i = 0; i < subItem.repeats; i++)
                    {
                        InvokeItem(target, subItem);
                        ApplyOnHitItems(target, action); // check if is hit

                        // should wait for animations and stuff
                    }
                }
                UseFollowUpItems(action);
            }
            /
        }

    public void InvokeItem(GameBattler a_target, DataUsableItem a_item)
    {
        // check for counter and magic reflection
        ApplyItemEffects(a_target, a_item); // check for substitute
        // set last target

    }

    public void ApplyItemEffects(GameBattler a_target, DataUsableItem a_item)
    {
        a_target.ItemApply(m_subject, a_item);
        // refresh status
        FindObjectOfType<CanvasBattle>().DisplayResults(a_target.actionResult);
        // wait for action results?
    }

    public void ApplyOnHitItems(GameBattler a_target, GameAction a_rootAction)
    {
        if (a_rootAction.item.damage.isNone) return; // check if was an actual hit
        foreach (DataUsableItem item in a_rootAction.subject.onHitItems)
        {
            GameAction action = new GameAction(a_rootAction.subject, item);
            List<GameBattler> targets = action.MakeOnHitTargets(a_rootAction, a_target);
            foreach (GameBattler target in targets)
            {
                InvokeItem(target, item);
            }
        }
    }

    public void StartPartyCommandSelection()
    {
        Debug.Log("Party command selection");
        if (c_actInOrder)
        {
            TurnStart();
            return;
        }
        canvas.OpenPartyStatus();
        if (InputStart())
        {
            canvas.StartPartyCommandSelection();
            m_currentSelection = Selection.party;
        }
        else
        {
            // party command window deactivate
            TurnStart();
        }  
    }

    public void StartActorCommandSelection()
    {
        Debug.Log("Starting actor command selection");
        canvas.StartActorCommandSelection();
        m_currentSelection = Selection.actor;
    }

    public void CommandAttack()
    {
        OnSkillOk(0);
    }

    public void CommandSkill()
    {
        canvas.SetSkillWindowActor(m_subject);
        // skill window show and activate
    }

    public void OnSkillOk()
    {
        //OnSkillOk(skill id);
    }

    public void OnSkillOk(int a_skillId)
    {
        GameActor user = c_actInOrder ? (m_subject as GameActor) : actor;
        user.input.SetSkill(a_skillId); // should set action
        // set last skill
        if (DataManager.skills[a_skillId].needSelection)
        {
            if (DataManager.skills[a_skillId].isForOpponent) { SelectEnemySelection(); }
            else { SelectActorSelection(); }
        }
        else
        {
            NextCommand();
        }
        
    }
    
    */
    }
